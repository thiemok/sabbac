var _game_manager_8cs =
[
    [ "Save", "struct_sabbac_1_1_save.html", "struct_sabbac_1_1_save" ],
    [ "GameManager", "class_sabbac_1_1_game_manager.html", "class_sabbac_1_1_game_manager" ],
    [ "DifficultyLevel", "_game_manager_8cs.html#a2393cc1caa5e36dea224abf14ad6d118", [
      [ "Level_Easy", "_game_manager_8cs.html#a2393cc1caa5e36dea224abf14ad6d118ab8fbf4dfd1eed5b5a937066e3c29a07a", null ],
      [ "Level_Normal", "_game_manager_8cs.html#a2393cc1caa5e36dea224abf14ad6d118a969a28f3c90a015c11f3878b2b35aed8", null ],
      [ "Level_Hard", "_game_manager_8cs.html#a2393cc1caa5e36dea224abf14ad6d118a75b38c33f5fe90564b7ac179c4f1e424", null ]
    ] ],
    [ "GameMode", "_game_manager_8cs.html#a5bd59a4d7e0aa6a53cb3fad157e5a031", [
      [ "MODE_RANDOM_MAZE", "_game_manager_8cs.html#a5bd59a4d7e0aa6a53cb3fad157e5a031a51cb682c2f82729c59850b51bc901cd3", null ],
      [ "MODE_LOAD_MAZE", "_game_manager_8cs.html#a5bd59a4d7e0aa6a53cb3fad157e5a031afacd6906944e2f299a13c90b9bdbefa9", null ]
    ] ],
    [ "MazeSize", "_game_manager_8cs.html#af823452bae75c13baa0c5b84ea6136ab", [
      [ "Size_Small", "_game_manager_8cs.html#af823452bae75c13baa0c5b84ea6136aba2ec9e2b82a42c47e87c42836c81bf675", null ],
      [ "Size_Medium", "_game_manager_8cs.html#af823452bae75c13baa0c5b84ea6136aba7fdad486dc69e6875b0db7291a1e0d1e", null ],
      [ "Size_Large", "_game_manager_8cs.html#af823452bae75c13baa0c5b84ea6136abacd07c9ff0ec6bee57227feb7086b318f", null ]
    ] ]
];