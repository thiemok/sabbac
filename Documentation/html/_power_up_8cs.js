var _power_up_8cs =
[
    [ "PowerUp", "class_sabbac_1_1_power_up.html", "class_sabbac_1_1_power_up" ],
    [ "PowerUpType", "_power_up_8cs.html#a55bf7cb3e4909638ae7fe08fb89bf0a9", [
      [ "Type_TimeReduction", "_power_up_8cs.html#a55bf7cb3e4909638ae7fe08fb89bf0a9a376cd54ae8f331e0f6994b8b468bc3ca", null ],
      [ "Type_SpeedBoost", "_power_up_8cs.html#a55bf7cb3e4909638ae7fe08fb89bf0a9aabe7e6040703a70d472b681daeed43b8", null ],
      [ "Type_BonusLive", "_power_up_8cs.html#a55bf7cb3e4909638ae7fe08fb89bf0a9a032a3641bb965e262f3f32525cae1a4f", null ],
      [ "Type_NoGravity", "_power_up_8cs.html#a55bf7cb3e4909638ae7fe08fb89bf0a9a76ec84b425756f472118549c5990f3ab", null ]
    ] ]
];