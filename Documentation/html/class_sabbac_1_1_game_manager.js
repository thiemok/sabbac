var class_sabbac_1_1_game_manager =
[
    [ "GameManager", "class_sabbac_1_1_game_manager.html#aa6f304db67cc3335a6d8378b24544d31", null ],
    [ "ApplyPowerUp", "class_sabbac_1_1_game_manager.html#a06d691d9b6b2f9aeebd356de00384b3a", null ],
    [ "ChangeEffectsVolume", "class_sabbac_1_1_game_manager.html#ab1f612c3e6e47a266acb4da323ce0a96", null ],
    [ "ChangeMusicVolume", "class_sabbac_1_1_game_manager.html#a667fdd29d595cd98ee27ebb1212faa56", null ],
    [ "EndGame", "class_sabbac_1_1_game_manager.html#a2f9a3233eb913407ee5d52c2a82861ac", null ],
    [ "FailLevel", "class_sabbac_1_1_game_manager.html#a74e83f2ae298a1f9f3254cdb7be68a42", null ],
    [ "GetDifficulty", "class_sabbac_1_1_game_manager.html#abfff30f24a95e25796077612fd1d6a03", null ],
    [ "GetEffectsVolume", "class_sabbac_1_1_game_manager.html#ab037583cd81a29e3bc4db7f5c8302a8b", null ],
    [ "GetGameToload", "class_sabbac_1_1_game_manager.html#a5138f79119d00206220434512b9b585a", null ],
    [ "GetHighscores", "class_sabbac_1_1_game_manager.html#a6dd768eb0e944edcd624f3234561951b", null ],
    [ "GetLevelTime", "class_sabbac_1_1_game_manager.html#a7bbce48ba4123c43327b0c89114f8c85", null ],
    [ "GetMaze", "class_sabbac_1_1_game_manager.html#a89bded107c0fa12d8ef4e7b680cb9ab1", null ],
    [ "GetMazeSize", "class_sabbac_1_1_game_manager.html#afa1fe695c69a78a0e71d3871d97393a2", null ],
    [ "GetMusicVolume", "class_sabbac_1_1_game_manager.html#a2d87e83ace0076a7129c0f1824fe8be5", null ],
    [ "GetPlayer", "class_sabbac_1_1_game_manager.html#afb708e64781e8c3d8886603c20902f70", null ],
    [ "GetPlayerName", "class_sabbac_1_1_game_manager.html#a5e6ada97a2ea6e1cb9665acb9a6bd85d", null ],
    [ "GetPlayerPosMaze", "class_sabbac_1_1_game_manager.html#af8f9fbb4a5d6bcdfb45292af52d3ad4b", null ],
    [ "GetResolutionChoice", "class_sabbac_1_1_game_manager.html#ac61d76c8d51b4914badea6614148ad59", null ],
    [ "GetSavedGame", "class_sabbac_1_1_game_manager.html#ae483ff0f606d1d3c3c1bd7af4cdd57e9", null ],
    [ "GetSavedGameTitles", "class_sabbac_1_1_game_manager.html#ac9362da32ea1679828a575a9b6278f6f", null ],
    [ "HasBeenVisited", "class_sabbac_1_1_game_manager.html#aca5feadc0fa76831b42454e07de150ef", null ],
    [ "IsFullscreen", "class_sabbac_1_1_game_manager.html#a01054bad22dbf43d71f4ac865506ed00", null ],
    [ "LevelIsRunning", "class_sabbac_1_1_game_manager.html#a6de95011c331ddaf9eaed1d788b87981", null ],
    [ "LoadMainMenu", "class_sabbac_1_1_game_manager.html#aee546ea8e8d95f9d4b3b0a86c39330ca", null ],
    [ "LoadSettings", "class_sabbac_1_1_game_manager.html#a8c9100590848d0b725e8dccd99b6a858", null ],
    [ "PauseGame", "class_sabbac_1_1_game_manager.html#a512c3dc8213ce381bda1e56b4ff16c7c", null ],
    [ "ReportPlayerPosition", "class_sabbac_1_1_game_manager.html#a69ca1d91d3d4f3e431548e1774056ced", null ],
    [ "RestartLevel", "class_sabbac_1_1_game_manager.html#a60d3644d73ae10ad42e2782a9b1cf48d", null ],
    [ "ResumeGame", "class_sabbac_1_1_game_manager.html#af16582db818d605e649e8d979f500a08", null ],
    [ "SaveGame", "class_sabbac_1_1_game_manager.html#a0df68ad87485f49ee4908095f6df96f6", null ],
    [ "SaveSettings", "class_sabbac_1_1_game_manager.html#a44601e724012ccb5574691b26e026d4e", null ],
    [ "SerializeGame", "class_sabbac_1_1_game_manager.html#a2b910959225ee83a8ce189c42b4c3008", null ],
    [ "SetDifficulty", "class_sabbac_1_1_game_manager.html#a6cb95ba49a2fff4236a2d3c378af86df", null ],
    [ "SetEffectsVolume", "class_sabbac_1_1_game_manager.html#ad367a94601ab32aa2ec9d536d1d76a68", null ],
    [ "SetFullscreen", "class_sabbac_1_1_game_manager.html#ab68b3d693211823d822e2f2c45f77291", null ],
    [ "SetGameMode", "class_sabbac_1_1_game_manager.html#a14080c976a4e65ba89d1bce653e84b50", null ],
    [ "SetGameToLoad", "class_sabbac_1_1_game_manager.html#ad7f440352891cc829a5fd16568d9b017", null ],
    [ "SetGameToLoad", "class_sabbac_1_1_game_manager.html#adcbd47cd6bb57febc1abe9dbe36fc3ac", null ],
    [ "SetMazeSize", "class_sabbac_1_1_game_manager.html#a0a4c4d3b1e3fa9e4548fc265d866a976", null ],
    [ "SetMusicVolume", "class_sabbac_1_1_game_manager.html#a3c0985dda52ff7c5800c05c0f9de1971", null ],
    [ "SetPlayerName", "class_sabbac_1_1_game_manager.html#a97451c889b30d11d5643b1b0b5fc192d", null ],
    [ "SetResolution", "class_sabbac_1_1_game_manager.html#aa11df03f098cd9bcad259e260403054c", null ],
    [ "StartLevel", "class_sabbac_1_1_game_manager.html#a0b0c66cc5d7d0b9e6d3c704f0b810172", null ],
    [ "TogglePause", "class_sabbac_1_1_game_manager.html#a4e4b5e90f268f63303d30b7f61630dbc", null ]
];