var searchData=
[
  ['skin',['skin',['../class_sabbac_1_1_game_menu_manager.html#a3d1db1b7d506cf3714e9db5f64108023',1,'Sabbac.GameMenuManager.skin()'],['../class_sabbac_1_1_main_menu_manager.html#aa67b3501ebc226156248b07e640aa613',1,'Sabbac.MainMenuManager.skin()'],['../class_sabbac_1_1_o_s_d_manager.html#a7e89ff4ba2f9f65e67fea0155473704a',1,'Sabbac.OSDManager.skin()']]],
  ['speed_5fboost_5fcolor',['SPEED_BOOST_COLOR',['../class_sabbac_1_1_power_up.html#afa7ef40686358f45d3f4349eef713355',1,'Sabbac::PowerUp']]],
  ['speed_5fboost_5fduration',['SPEED_BOOST_DURATION',['../class_sabbac_1_1_power_up.html#a5cdd8a903162479c4b9c98a1c1a2617b',1,'Sabbac::PowerUp']]],
  ['speed_5fboost_5feffect',['SPEED_BOOST_EFFECT',['../class_sabbac_1_1_power_up.html#ae8e1d1255f55512f0de25da666a1fc71',1,'Sabbac::PowerUp']]],
  ['start',['start',['../class_sabbac_1_1_level_builder.html#acc886e4cc2b84438ce09b5f9402b35ee',1,'Sabbac::LevelBuilder']]]
];
