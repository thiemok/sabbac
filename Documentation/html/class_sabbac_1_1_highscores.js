var class_sabbac_1_1_highscores =
[
    [ "Score", "struct_sabbac_1_1_highscores_1_1_score.html", "struct_sabbac_1_1_highscores_1_1_score" ],
    [ "Highscores", "class_sabbac_1_1_highscores.html#a50a72df3bfa848d56c1c619b0becc009", null ],
    [ "AddScore", "class_sabbac_1_1_highscores.html#a000775aeb2950538f694ae7715f73b67", null ],
    [ "GetScores", "class_sabbac_1_1_highscores.html#af832d285857bd03f3b76540b9bb70ca4", null ],
    [ "IsHighScore", "class_sabbac_1_1_highscores.html#a8adf991ee87704ab8c80f18710628223", null ]
];