var class_sabbac_1_1_player =
[
    [ "AddBonusLive", "class_sabbac_1_1_player.html#ae9d38eb10189bda379ee549c194ea30a", null ],
    [ "BoostSpeed", "class_sabbac_1_1_player.html#a6eabbd2e70d5939cf93aa11db0756673", null ],
    [ "GetRemainingLives", "class_sabbac_1_1_player.html#a3699c60e6c7b4ba31f9b77e205217876", null ],
    [ "IsRespawning", "class_sabbac_1_1_player.html#ae06654b678717c20988b725d167a4ad3", null ],
    [ "LooseLive", "class_sabbac_1_1_player.html#a03e6317a5d650ef5633bf7fdb7fb5361", null ],
    [ "NoGravity", "class_sabbac_1_1_player.html#a3e38ff42f6bee7e308a7b1b27cdbcfc0", null ],
    [ "Respawn", "class_sabbac_1_1_player.html#ac42e3ed3b424becf91f0ba068f90d7b3", null ],
    [ "cameraRef", "class_sabbac_1_1_player.html#ad2ae34d3b53f532dad8e99c4df78dae3", null ],
    [ "jumpForce", "class_sabbac_1_1_player.html#a261c1c80cecde10377b9b057e036c57c", null ],
    [ "movementForce", "class_sabbac_1_1_player.html#a569de5e6d845086b0c5a5ac83ccd0b0a", null ]
];