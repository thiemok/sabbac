﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Dummy Player Skript, verwendet um den Spielrcharakter ohne eigentliche Funktionalität anzuzeigen.
/// </summary>
public class DummyPlayer : MonoBehaviour {

	private static readonly int PRATICLES_EMITTED_PER_PULSE = 8;

	private Light _light;
	private float lightMinIntensity;
	private float lightMaxIntensity;
	private bool lightIntensityIncreasing = false;

	// Use this for initialization
	void Start () {
	
		this._light = GetComponentInChildren<Light> ();
		this.lightMaxIntensity = this._light.intensity;
		this.lightMinIntensity = 1;

		StartCoroutine (DoLightAnimation ());
	}

	/// <summary>
	/// Führt die Licht animation aus.
	/// </summary>
	/// <returns>Coroutine.</returns>
	IEnumerator DoLightAnimation() {
		while (true) {
			float rand = UnityEngine.Random.value * 0.1f;
			if (!this.lightIntensityIncreasing) {
				rand = -rand;
			}
			
			this._light.intensity += rand;
			
			if (this._light.intensity > this.lightMaxIntensity) {
				this.lightIntensityIncreasing = false;
				GetComponentInChildren<ParticleSystem> ().Emit (PRATICLES_EMITTED_PER_PULSE);
			} else if (this._light.intensity < this.lightMinIntensity) {
				this.lightIntensityIncreasing = true;
			}
			
			yield return null;
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
