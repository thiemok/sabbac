﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Repräsentiert die Hauptkamera
/// </summary>
public class MouseAimCamera : MonoBehaviour {

	/// <summary>
	/// Das Ziel der Kamera.
	/// </summary>
	public GameObject target;
	/// <summary>
	/// Die Rotationsgeschwindigkeit der Kamera.
	/// </summary>
	public float rotationSpeed = 0.02f;
	//public float minDistToTarget = 0f;

	private float targetDistToTarget;
	private float offset;
	private Quaternion currentRot;
	private bool paused;
	private bool movingIn;
	private Vector3 vOffset;

	void Start() {
		this.paused = false;
		this.movingIn = false;
		this.offset = Vector3.Distance(transform.position, target.transform.position);
		this.targetDistToTarget = offset;
		this.vOffset = new Vector3(offset, 0, 0);
		this.currentRot = Quaternion.Euler (0, 180, 200);
	}

	void LateUpdate() {

		if (!this.paused) {

			/*
			if(movingIn) {
				float dist = Vector3.Distance(transform.position, target.transform.position); 
				offset = Mathf.Lerp (dist, minDistToTarget, Time.deltaTime);
			} else {
				float dist = Vector3.Distance(transform.position, target.transform.position); 
				offset = Mathf.Lerp (targetDistToTarget, dist, Time.deltaTime);
			}
			*/
			float yDest = currentRot.eulerAngles.y + Input.GetAxis ("Mouse X") * rotationSpeed;
			float zDest = currentRot.eulerAngles.z - Input.GetAxis ("Mouse Y") * rotationSpeed;
			zDest = Mathf.Clamp (zDest, 180, 245);
			currentRot = Quaternion.Euler(0, yDest, zDest);

			vOffset.x = offset;
			Vector3 direction = currentRot * vOffset;
			transform.position = target.transform.position - direction;
			transform.LookAt (target.transform.position);
		}

	}
	/*
	void OnCollisionEnter(Collision c) {

		Vector3 t = (transform.position - target.transform.position).normalized * minDistToTarget;
		transform.position = Vector3.Lerp (transform.position, t, Time.deltaTime);
		transform.LookAt (target.transform.position);

		string tag = c.gameObject.tag;
		if (tag != "Floor" || tag != "Player") {
			this.movingIn = true;
			Debug.Log ("trigger");
		}
	}

	void OnCollisionExit(Collision c) {

		Vector3 t = transform.position + targetDistToTarget;
		transform.position = Vector3.Lerp (t, transform.position, Time.deltaTime);
		transform.LookAt (target.transform.position);

		string tag = c.gameObject.tag;
		if (tag != "Floor" || tag != "Player") {
			this.movingIn = false;
			Debug.Log ("trigger");
		}
	}
	*/

	/// <summary>
	/// Benachrichtigt die Kamera über Pausierung des Spiels.
	/// </summary>
	public void TogglePause() {
		this.paused = !this.paused;
	}
}