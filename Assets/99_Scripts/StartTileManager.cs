﻿using UnityEngine;
using System.Collections;

namespace Sabbac {

	/// <summary>
	/// Verwaltet die Funktionen des Startfelds.
	/// </summary>
	public class StartTileManager : TileManager {

		/**
		 * Wird ausgelöst wenn der Spieler das Feld betritt, meldet die aktuelle Position und Respawnpunkt dem GameManager.
		 * @param c Der Collider der den Trigger auslöst
		 */
		new void OnTriggerEnter(Collider c) {
			if (c.attachedRigidbody.tag == "Player") {
				GameManager.Instance.ReportPlayerPosition (0, 0, this.respawnPoint);
			}
		}
	}
}
