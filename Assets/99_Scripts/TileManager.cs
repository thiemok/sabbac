﻿using UnityEngine;
using System.Collections;

namespace Sabbac {

	/// <summary>
	/// Verwaltet die allgemeinen Funktionen eines Labyrinthfelds.
	/// </summary>
	public class TileManager : MonoBehaviour {

		public GameManager gameManager;
		public int mazePosX;
		public int mazePosY;
		
		protected Vector3 respawnPoint;

		// Use this for initialization
		void Start () {
			this.respawnPoint = transform.FindChild("RespawnPoint").position;
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		/// <summary>
		/// Wird ausgelöst wenn der Spieler das Feld betritt, meldet die aktuelle Position und Respawnpunkt dem GameManager.
		/// </summary>
		/// <param name="c">Der Collider der den Trigger auslöst.</param>
		public virtual void OnTriggerEnter(Collider c){
			if (c.attachedRigidbody.tag == "Player") {
				this.gameManager.ReportPlayerPosition (this.mazePosX, this.mazePosY, this.respawnPoint);
			}
		}

		/// <summary>
		/// Meldet das nichtbestehen des Feldes an den GameManager.
		/// </summary>
		public void FailTile() {
			this.gameManager.FailLevel ();		
		}
	}
}
