﻿using UnityEngine;
using System.Collections;
using System;

namespace Sabbac {

	/// <summary>
	/// Repräsentiert die Highscores eines Levels.
	/// </summary>
	[Serializable]
	public class Highscores {

		/// <summary>
		/// Anzahl der Highscore einträge.
		/// </summary>
		private static int LIST_LENGTH = 5;

		[SerializeField] private string[] names;
		[SerializeField] private float[] scores;

		/// <summary>
		/// Repräsentation eines Highscore eintrags.
		/// </summary>
		public struct Score { 
			public string name;
			public float time;

			public Score (string n, float t) {
				name = n;
				time = t;
			}
		}

		/// <summary>
		/// Initialisiert ein neues Object der <see cref="Sabbac.Highscores"/> Klasse.
		/// </summary>
		public Highscores() {
			this.names = new string[LIST_LENGTH];
			this.scores = new float[LIST_LENGTH];
			for (int i = 0; i < LIST_LENGTH; i++) {
				this.names[i] = "";
				this.scores[i] = 0.0f;
			}
		}

		/// <summary>
		/// Liefert die Einträge.
		/// </summary>
		/// <returns>Die Einträge.</returns>
		public Score[] GetScores() {
			Score[] s = new Score[LIST_LENGTH];

			for (int i = 0; i < LIST_LENGTH; i++) {
				s[i] = new Score(this.names[i], this.scores[i]);	
			}
			return s;
		}

		/**
		 * Determines if the given score is better than the last entry
		 */
		/// <summary>
		/// Überprüft ob der übergebene <see cref="Sabbac.Score"/> unter den Highscores ist.
		/// </summary>
		/// <returns><c>true</c>, wenn der Score besser als der letzte Score der Highscore ist, sonst <c>false</c>.</returns>
		/// <param name="s">Der Score.</param>
		public bool IsHighScore(Score s) {
			return s.time < this.scores [LIST_LENGTH - 1] || this.scores[LIST_LENGTH - 1] == 0;
		}

		/**
		 * Adds the given score to the list highscores
		 */
		public void AddScore(Score s) {
			int i = LIST_LENGTH;

			while (i >  0 && (s.time < this.scores[i - 1] || this.scores[i - 1] == 0)) {
				if(i > LIST_LENGTH) {
					this.scores[i] = this.scores[i - 1];
					this.names[i] = this.names[i - 1];
				}
				i--;
			}

			if (i < LIST_LENGTH) {
				this.scores[i] = s.time;
				this.names[i] = s.name;
			}
		}
	}
}