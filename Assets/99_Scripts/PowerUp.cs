﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Sabbac {

	/// <summary>
	/// Die verfügbaren Typen der PowerUps.
	/// </summary>
	public enum PowerUpType {
		Type_TimeReduction,
		Type_SpeedBoost,
		Type_BonusLive,
		Type_NoGravity
	}

	/// <summary>
	/// Repräsentiert ein PowerUp.
	/// </summary>
	public class PowerUp : MonoBehaviour {
	
		/// <summary>
		/// Das Material für die Würfelseiten, das beim Typ Zeitbonus verwendet wird.
		/// </summary>
		public Material PANEL_TIME_REDUCTION_MATERIAL;
		/// <summary>
		/// Das Material für die Würfelseiten, das beim Typ Geschwindigkeitsbonus verwendet wird.
		/// </summary>
		public Material PANEL_SPEED_BOOST_MATERIAL;
		/// <summary>
		/// Das Material für die Würfelseiten, das beim Typ Extra Leben verwendet wird..
		/// </summary>
		public Material PANEL_BONUS_LIVE_MATERIAL;
		/// <summary>
		/// Das Material für die Würfelseiten, das beim Typ No Gravity verwendet wird.
		/// </summary>
		public Material PANEL_NO_GRAVITY_MATERIAL;
		/// <summary>
		/// Effektdauer für den Typ Zeitbonus.
		/// </summary>
		public static readonly float TIME_REDUCTION_EFFECT = 25;
		/// <summary>
		/// Farbe für den Typ Zeitbonus.
		/// </summary>
		public static readonly Color TIME_REDUCTION_COLOR = Color.blue;
		/// <summary>
		/// Stärke des Geschwindigkeitsbonus.
		/// </summary>
		public static readonly float SPEED_BOOST_EFFECT = 2;
		/// <summary>
		/// Effektdauer des Geschwindigkeitsbonus.
		/// </summary>
		public static readonly float SPEED_BOOST_DURATION = 10;
		/// <summary>
		/// Farbe des Typs Geschwindigkeitsbonus.
		/// </summary>
		public static readonly Color SPEED_BOOST_COLOR = Color.green;
		/// <summary>
		/// Farbe des Typs Extra Leben.
		/// </summary>
		public static readonly Color BONUS_LIVE_COLOR = Color.red;
		/// <summary>
		/// Effektdauer des Typs No Gravity.
		/// </summary>
		public static readonly float NO_GRAVITY_DURATION = 10;
		/// <summary>
		/// Farbe des Tyos No Gravity.
		/// </summary>
		public static readonly Color NO_GRAVITY_COLOR = Color.yellow;
		/// <summary>
		/// Rotationsgeschwindigkeit der Animation.
		/// </summary>
		public static readonly float ANIMATION_ROTATION_SPEED = 50f;

		private PowerUpType type;
		private Dictionary<PowerUpType, Material> panelMaterials;

		void Awake() {
			this.panelMaterials = new Dictionary<PowerUpType, Material>();

			PANEL_TIME_REDUCTION_MATERIAL.color = TIME_REDUCTION_COLOR;
			PANEL_BONUS_LIVE_MATERIAL.color = BONUS_LIVE_COLOR;
			PANEL_NO_GRAVITY_MATERIAL.color = NO_GRAVITY_COLOR;
			PANEL_SPEED_BOOST_MATERIAL.color = SPEED_BOOST_COLOR;

			this.panelMaterials.Add (PowerUpType.Type_TimeReduction, PANEL_TIME_REDUCTION_MATERIAL);
			this.panelMaterials.Add (PowerUpType.Type_SpeedBoost, PANEL_SPEED_BOOST_MATERIAL);
			this.panelMaterials.Add (PowerUpType.Type_BonusLive, PANEL_BONUS_LIVE_MATERIAL);
			this.panelMaterials.Add (PowerUpType.Type_NoGravity, PANEL_NO_GRAVITY_MATERIAL);
		}

		// Use this for initialization
		void Start () {
			this.type = HelperFunctions.GetRandomEnum<PowerUpType>();

			SetPanelMaterial ();
			StartCoroutine (DoAnimation ());
		}
	
		// Update is called once per frame
		void Update () {
	
		}

		void OnDestroy() {
			StopAllCoroutines ();		
		}

		/// <summary>
		/// Liefert den Typ des PowerUps.
		/// </summary>
		/// <returns>Der Typ.</returns>
		public PowerUpType GetPowerUpType() {
			return this.type;		
		}

		/// <summary>
		/// Setzt das Material für die Würfelseiten.
		/// </summary>
		private void SetPanelMaterial() {
			Material m = this.panelMaterials [this.type];
			Transform box = this.transform.FindChild ("Box");
			box.FindChild ("Panel_Back").renderer.material = m;
			box.FindChild ("Panel_Bottom").renderer.material = m;
			box.FindChild ("Panel_Front").renderer.material = m;
			box.FindChild ("Panel_Left").renderer.material = m;
			box.FindChild ("Panel_Right").renderer.material = m;
			box.FindChild ("Panel_Top").renderer.material = m;
		}

		/// <summary>
		/// Für die Animation des PowerUps aus.
		/// </summary>
		/// <returns>Die Animation.</returns>
		private IEnumerator DoAnimation() {
			while (true) {
			
				transform.Rotate(0, ANIMATION_ROTATION_SPEED * Time.deltaTime, 0);

				yield return null;
			}
		}
	}
}
