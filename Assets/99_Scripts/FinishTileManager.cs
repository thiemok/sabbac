﻿using UnityEngine;
using System.Collections;

namespace Sabbac {

	/// <summary>
	/// Verwaltet die allgemeinen Funktionen des Zielfelds.
	/// </summary>
	public class FinishTileManager : TileManager {

		/// <summary>
		/// Wird ausgelöst wenn der Spieler das Feld betritt, meldet die aktuelle Position und Respawnpunkt dem GameManager.
		/// </summary>
		/// <param name="c">Der Collider der den Trigger auslöst.</param>
		new void OnTriggerEnter(Collider c) {
			if (c.attachedRigidbody.tag == "Player") {
				base.OnTriggerEnter (c);

				this.gameManager.EndGame (true);
			}
		}
	}
}
