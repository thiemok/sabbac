﻿using UnityEngine;
using System;
using System.Collections;

namespace Sabbac {

	/// <summary>
	/// Enthält verschiedene Hilfsfunktionen.
	/// </summary>
	public class HelperFunctions {
	
		/// <summary>
		/// Liefert einen Zufälligen Member der übergebenen Enumertation.
		/// </summary>
		/// <returns>Der zufällige Member.</returns>
		/// <typeparam name="T">Die Enemuration.</typeparam>
		public static T GetRandomEnum<T>() {
			System.Array values = System.Enum.GetValues(typeof(T));
			T value = (T) values.GetValue(UnityEngine.Random.Range(0, values.Length));
			return value;
		}

		/// <summary>
		/// Führt die übergebene Aktion nach der übergebenen Zeit durch.
		/// </summary>
		/// <returns>Coroutine</returns>
		/// <param name="wait">Die Wartezeit in Sekunden.</param>
		/// <param name="whatToDo">Die Aktion.</param>
		public static IEnumerator DoAfterWait(float wait, Action whatToDo) {
			yield return new WaitForSeconds (wait);
			whatToDo ();
		}
	}
}
