﻿using UnityEngine;
using System.Collections;
using Sabbac;

/// <summary>
/// Repräsentiert das spezielle Verhalten des ChamberBar Felds.
/// </summary>
public class ChamberBarBehaviour : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/// <summary>
	/// Wird ausgelöst, wenn etwas in das Loch fällt.
	/// </summary>
	/// <param name="c">Der Collider, der in das Loch gefallen ist.</param>
	void OnTriggerEnter(Collider c) {
		if (c.attachedRigidbody.tag == "Player") {
			TileManager t = (TileManager)GetComponentInParent (typeof(TileManager));
			t.FailTile ();
		}
	}
	
}
