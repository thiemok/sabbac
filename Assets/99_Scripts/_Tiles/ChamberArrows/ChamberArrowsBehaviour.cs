﻿using UnityEngine;
using System.Collections;
using Sabbac;

/// <summary>
/// Repräsentiert das spezielle Verhalten eines CHamberArrow Felds.
/// </summary>
public class ChamberArrowsBehaviour : MonoBehaviour {

	/// <summary>
	/// Das GameObject der abzufeuernden Pfeile.
	/// </summary>
	public GameObject ARROW;
	/// <summary>
	/// Die Anzahl der Spawnpunkte für Pfeile.
	/// </summary>
	public int NO_OF_ARROW_SPAWN_POINTS;
	/// <summary>
	/// Die Kraft, mit der Pfeile abgefeuert werden sollen.
	/// </summary>
	public float ARROW_FORCE;
	/// <summary>
	/// Das Interval, das zwischen den Pfeilabschüssen vergehen soll.
	/// </summary>
	public float ARROW_INTERVAL;

	private Transform[] arrowSpawnPoints;
	private bool spawning = false;
	private Vector3 arrowVector;
	private TileManager tileManager;
	private float arrowIntervalMultiplier = 1;

	// Use this for initialization
	void Start () {
	
		this.arrowSpawnPoints = new Transform[NO_OF_ARROW_SPAWN_POINTS];
		for (int i = 1; i <= NO_OF_ARROW_SPAWN_POINTS; i++) {
			Transform sp = transform.FindChild("Spawn_Arrow_" + i);
			this.arrowSpawnPoints[i -1] = sp;
		}

		this.arrowVector = transform.rotation * (-Vector3.forward * ARROW_FORCE);
		this.tileManager = (TileManager)this.GetComponent (typeof(TileManager));
		//StartCoroutine(SpawnArrows()); //Zu testzwecken Löschen wenn alles funktioniert

		if (GameManager.Instance.GetDifficulty() != DifficultyLevel.Level_Easy) {
			this.arrowIntervalMultiplier = 1.5f;		
		}
	}
	
	// Update is called once per frame
	void Update () {

		Vector2 tilePos = new Vector2 (tileManager.mazePosX, tileManager.mazePosY);
		Vector2 playerPos = GameManager.Instance.GetPlayerPosMaze ();

		if (!spawning && tilePos == playerPos) {
				
			spawning = true;
			StartCoroutine(SpawnArrows());
			//StartCoroutine("SpawnArrows");
		} else if (spawning && tilePos != playerPos) {
				
			spawning = false;;
			//StopCoroutine(SpawnArrows());
			//StartCoroutine("SpawnArrows");
		}
	}

	/// <summary>
	/// Zuständig für das abschießen der Pfeile.
	/// </summary>
	/// <returns>Coroutine.</returns>
	private IEnumerator SpawnArrows() {
		int spIndex = 0;

		//while (true) {
		while(spawning){
			GameObject arrow = (GameObject)GameObject.Instantiate(ARROW, arrowSpawnPoints[spIndex].position, Quaternion.identity);
			((ArrowBehaviour)arrow.GetComponent(typeof(ArrowBehaviour))).tm = tileManager;
			arrow.rigidbody.AddForce(arrowVector, ForceMode.Impulse);

			spIndex = (spIndex + 1) % NO_OF_ARROW_SPAWN_POINTS;
			yield return new WaitForSeconds(ARROW_INTERVAL * arrowIntervalMultiplier);
		}
		yield return null;
	}
}
