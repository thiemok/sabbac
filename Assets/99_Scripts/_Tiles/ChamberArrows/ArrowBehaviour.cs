﻿using UnityEngine;
using System.Collections;
using Sabbac;

/// <summary>
/// Repräsentiert das Verhalten eines Pfeils.
/// </summary>
public class ArrowBehaviour : MonoBehaviour {

	/// <summary>
	/// Der TileManager des Feldes, welches den Pfeil abgefeuert hat.
	/// </summary>
	public TileManager tm;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Wird ausgelöst, wenn der Pfeil mit etwas Kollidiert.
	/// </summary>
	/// <param name="c">Der Collider, mit dem der Prfeil zusammengestoßen ist.</param>
	void OnCollisionEnter(Collision c) {
		if (c.gameObject.tag == "Player") {
			tm.FailTile ();
		} else {
			StartCoroutine(HelperFunctions.DoAfterWait(2.0f, delegate() {
				Destroy(gameObject);
			}));
		}
	}
}
