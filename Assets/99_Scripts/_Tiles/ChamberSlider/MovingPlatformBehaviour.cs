﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Repräsentiert das Verhalten einer sich bewegenden Plattform.
/// </summary>
public class MovingPlatformBehaviour : MonoBehaviour {

	private GameObject player;
	private bool playerIsOnPlatform = false;
	private Vector3 lastPosition;

	// Use this for initialization
	void Start () {
		this.player = GameObject.Find("Player");
		this.lastPosition = this.transform.position;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		Vector3 deltaMovement = this.transform.position - this.lastPosition;

		if (this.playerIsOnPlatform) {
			player.transform.position += deltaMovement;		
		}

		this.lastPosition = this.transform.position;
	}

	/// <summary>
	/// Wird ausgelöst, wenn etwas die Plattform bertitt.
	/// </summary>
	/// <param name="c">Der Collider, der die Plattform betritt..</param>
	void OnCollisionEnter(Collision c) {
		if (c.gameObject == this.player) {
			this.playerIsOnPlatform = true;		
		}
	}

	/// <summary>
	/// Wird ausgelöst, wenn etwas die Plattfor verlässt.
	/// </summary>
	/// <param name="c">Der Collider, der die Plattform verlässt.</param>
	void OnCollisionExit(Collision c) {
		if (c.gameObject == this.player) {
			this.playerIsOnPlatform = false;		
		}
	}
}
