﻿using UnityEngine;
using System.Collections;
using Sabbac;

/// <summary>
/// Repräsentiert das Spezielle verhalten des ChamberSlider Felds.
/// </summary>
public class ChamberSliderBehaviour : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		if (GameManager.Instance.GetDifficulty() != DifficultyLevel.Level_Easy) {
			//animation["Slider_Action"].speed = 1.5f;
			((Animation)GetComponentInParent(typeof(Animation)))["Slider_Action"].speed = 1.5f;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/// <summary>
	/// Wird ausgelöst, wenn etwas in das Loch fällt.
	/// </summary>
	/// <param name="c">Der Collider, der in das Loch gefallen ist.</param>
	void OnTriggerEnter(Collider c) {
		if (c.attachedRigidbody.tag == "Player") {
			TileManager t = (TileManager)GetComponentInParent (typeof(TileManager));
			t.FailTile ();
		}
	}
	
}

