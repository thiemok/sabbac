﻿using UnityEngine;
using System.Collections;

namespace Sabbac {

	/// <summary>
	/// Verwaltet das Hauptmenü.
	/// </summary>
	public class MainMenuManager : MonoBehaviour {

		private enum MenuState {
			State_Main,
			State_NewGame,
			State_LoadGame,
			State_Options,
			State_Credits
		}

		/// <summary>
		/// Der Titel des Spiels.
		/// </summary>
		public readonly static string GAME_TITLE = "Sabbac";
		/// <summary>
		/// Schriftgröße des Titels.
		/// </summary>
		public readonly static int TITLE_LABEL_FONT_SIZE = 60;
		/// <summary>
		/// Schriftgröße für Überschriften.
		/// </summary>
		public readonly static int HEADING_LABEL_FONT_SIZE = 24;
		/// <summary>
		/// Schriftgröße für die Überschriften der Menüoptionen.
		/// </summary>
		public readonly static int OPTION_HEADING_LABEL_FONT_SIZE = 18;
		/// <summary>
		/// Beschriftungen der Schwierigkeitsgrade.
		/// </summary>
		public readonly static string[] DIFFICULTY_LABELS = new string[] {"Einfach", "Normal", "Schwer"};
		/// <summary>
		/// Beschriftungen der Labyrinthgrößen.
		/// </summary>
		public readonly static string[] MAZE_SIZE_LABELS = new string[] {"Klein", "Mittel", "Groß"};
		/// <summary>
		/// Skin des Menüs.
		/// </summary>
		public GUISkin skin;
		/// <summary>
		/// Audiosource für die Menümusik.
		/// </summary>
		public AudioSource menuMusicSource;

		private GameManager gameManager;
		private MenuState menuState;
		private int screenDivider = 8;
		private int menuPosMultiplier;
		private int loadGameIndex;
		private TextAsset credits;
		private string playerName;

		void Awake() {
			this.gameManager = GameManager.Instance;
		}

		void Start() {

			this.menuMusicSource.ignoreListenerPause = true;
			this.menuMusicSource.ignoreListenerVolume = true;
			this.menuMusicSource.loop = true;
			this.menuMusicSource.volume = this.gameManager.GetMusicVolume();

			this.playerName = this.gameManager.GetPlayerName ();

			this.menuState = MenuState.State_Main;

			this.credits = (TextAsset)Resources.Load ("Credits", typeof(TextAsset));

			this.menuPosMultiplier = this.screenDivider / 6; //Do not change
		}

		/// <summary>
		/// Erstellt das grundlegende Layout des Menüs und wechselt zwischen den einzelnen Inhalten.
		/// </summary>
		void OnGUI() {

			GUI.skin = this.skin;

			GUILayout.BeginArea (
				new Rect (
					(Screen.width / this.screenDivider) * this.menuPosMultiplier,
					(Screen.height / this.screenDivider) * this.menuPosMultiplier,
					(Screen.width / this.screenDivider) * (this.screenDivider - 2 * this.menuPosMultiplier),
					(Screen.height / this.screenDivider) * (this.screenDivider - 2 * this.menuPosMultiplier)
				),
				GUI.skin.box
			);

			GUILayout.BeginVertical ();

			GUIStyle tls = new GUIStyle (GUI.skin.label);
			tls.fontSize = TITLE_LABEL_FONT_SIZE;
			tls.alignment = TextAnchor.MiddleCenter;
			GUILayout.Label (GAME_TITLE, tls);

			switch (this.menuState) {
			case MenuState.State_Main:
				DisplayMainMenu();
				break;
			
			case MenuState.State_NewGame:
				DisplayNewGameMenu();
				break;

			case MenuState.State_LoadGame:
				DisplayLoadGameMenu();
				break;

			case MenuState.State_Options:
				DisplayOptionsMenu();
				break;

			case MenuState.State_Credits:
				DisplayCredits();
				break;
			}

			GUILayout.EndVertical ();
			GUILayout.EndArea ();
		}

		/// <summary>
		/// Zeichnet das Hauptmenü.
		/// </summary>
		private void DisplayMainMenu() {
			GUILayout.BeginVertical ();

			GUILayout.FlexibleSpace ();

			if (GUILayout.Button ("Neues Spiel")) {
				audio.Play();
				this.menuState = MenuState.State_NewGame;
			}

			if (GUILayout.Button ("Spiel laden")) {
				audio.Play();
				this.menuState = MenuState.State_LoadGame;
			}

			if (GUILayout.Button ("Optionen")) {
				audio.Play();
				this.menuState = MenuState.State_Options;
			}

			if (GUILayout.Button ("Credits")) {
				audio.Play ();
				this.menuState = MenuState.State_Credits;
			}

			GUILayout.FlexibleSpace ();

			if (GUILayout.Button ("Beenden")) {
				audio.Play();
				StartCoroutine(Quit ());
			}

			GUILayout.EndVertical ();
		}

		/// <summary>
		/// Zeichnet das neues Spiel Menü.
		/// </summary>
		private void DisplayNewGameMenu() {
			GUILayout.BeginVertical ();

			GUIStyle hls = new GUIStyle (GUI.skin.label);
			hls.fontSize = HEADING_LABEL_FONT_SIZE;
			hls.alignment = TextAnchor.MiddleCenter;
			GUILayout.Label("Neues Spiel", hls);

			GUILayout.FlexibleSpace ();

			hls.fontSize = OPTION_HEADING_LABEL_FONT_SIZE;
			GUILayout.Label("Schwierigkeitsgrad", hls);
			this.gameManager.SetDifficulty((DifficultyLevel)GUILayout.SelectionGrid (
				(int)this.gameManager.GetDifficulty(),
				DIFFICULTY_LABELS,
				DIFFICULTY_LABELS.Length
			));

			GUILayout.FlexibleSpace ();

			GUILayout.Label("Labyrintgröße", hls);
			this.gameManager.SetMazeSize((MazeSize)GUILayout.SelectionGrid (
				(int)this.gameManager.GetMazeSize(),
				MAZE_SIZE_LABELS,
				MAZE_SIZE_LABELS.Length
			));

			GUILayout.FlexibleSpace ();

			if (GUILayout.Button ("Spiel starten")) {
				audio.Play();
				this.gameManager.SetGameMode(GameMode.MODE_RANDOM_MAZE);
				StartCoroutine(StartGame());
			}

			if (GUILayout.Button ("Zurück")) {
				audio.Play();
				this.menuState = MenuState.State_Main;
			}

			GUILayout.EndVertical ();
		}

		/// <summary>
		/// Zeichnet das Spiel laden Menü.
		/// </summary>
		private void DisplayLoadGameMenu() {
			GUILayout.BeginVertical ();

			GUIStyle hls = new GUIStyle (GUI.skin.label);
			hls.fontSize = HEADING_LABEL_FONT_SIZE;
			hls.alignment = TextAnchor.MiddleCenter;
			GUILayout.Label("Spiel Laden", hls);
			
			GUILayout.FlexibleSpace ();

			hls.fontSize = OPTION_HEADING_LABEL_FONT_SIZE;
			GUILayout.Label("Speicherstände", hls);

			this.loadGameIndex = GUILayout.SelectionGrid (this.loadGameIndex, this.gameManager.GetSavedGameTitles(), 1);

			GUILayout.FlexibleSpace ();

			if (this.loadGameIndex == this.gameManager.GetSavedGameTitles().Length - 1) {
				this.gameManager.SetGameToLoad(GUILayout.TextField(this.gameManager.GetGameToload()));
				GUILayout.FlexibleSpace();
			}

			if (GUILayout.Button ("Spiel laden")) {
				audio.Play();
				this.gameManager.SetGameMode(GameMode.MODE_LOAD_MAZE);
				if (this.loadGameIndex != this.gameManager.GetSavedGameTitles().Length - 1) {
					Save save = this.gameManager.GetSavedGame(this.loadGameIndex);
					this.gameManager.SetGameToLoad(save.data);
				}
				StartCoroutine(StartGame());
			}

			if (GUILayout.Button ("Zurück")) {
				audio.Play ();
				this.menuState = MenuState.State_Main;			
			}

			GUILayout.EndVertical ();
		}

		/// <summary>
		/// Zeichnet das Optionen Menü.
		/// </summary>
		private void DisplayOptionsMenu() {
			GUILayout.BeginVertical ();

			GUIStyle hls = new GUIStyle (GUI.skin.label);
			hls.fontSize = HEADING_LABEL_FONT_SIZE;
			hls.alignment = TextAnchor.MiddleCenter;
			GUILayout.Label("Optionen", hls);

			GUILayout.FlexibleSpace ();

			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label ("Spielername");
			GUILayout.FlexibleSpace ();
			this.playerName = GUILayout.TextField (this.playerName, GUILayout.MinWidth(125));
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();

			GUILayout.FlexibleSpace ();

			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label("Effekt Laustärke");
			GUILayout.FlexibleSpace ();
			GUILayout.Box ((int)(this.gameManager.GetEffectsVolume() * 100) + " %");
			if (GUILayout.Button ("-")) {
				this.gameManager.ChangeEffectsVolume(-0.05f);
				audio.Play();
			}
			if (GUILayout.Button ("+")) {
				this.gameManager.ChangeEffectsVolume(0.05f);
				audio.Play();
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();

			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label("Musik Laustärke");
			GUILayout.FlexibleSpace ();
			GUILayout.Box ((int)(this.gameManager.GetMusicVolume() * 100) + " %");
			if (GUILayout.Button ("-")) {
				audio.Play();
				this.gameManager.ChangeMusicVolume(-0.05f);
				this.menuMusicSource.volume = this.gameManager.GetMusicVolume();
			}
			if (GUILayout.Button ("+")) {
				audio.Play();
				this.gameManager.ChangeMusicVolume(0.05f);
				this.menuMusicSource.volume = this.gameManager.GetMusicVolume();
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();

			GUILayout.FlexibleSpace ();

			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			GUILayout.Label("Auflösung");
			GUILayout.FlexibleSpace ();
			Resolution r = Screen.resolutions[this.gameManager.GetResolutionChoice()];
			GUILayout.Box("" + r.width + "x" + r.height + ", " + r.refreshRate);
			if (GUILayout.Button ("-")) {
				audio.Play();
				this.gameManager.SetResolution(this.gameManager.GetResolutionChoice() - 1);
			}
			if (GUILayout.Button ("+")) {
				audio.Play();
				this.gameManager.SetResolution(this.gameManager.GetResolutionChoice() + 1);
			}
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();

			GUILayout.FlexibleSpace ();

			GUILayout.BeginHorizontal ();
			GUILayout.FlexibleSpace ();
			this.gameManager.SetFullscreen(GUILayout.Toggle(this.gameManager.IsFullscreen(), "Fullscreen aktivieren?"));
			GUILayout.FlexibleSpace ();
			GUILayout.EndHorizontal ();

			GUILayout.FlexibleSpace ();

			if (GUILayout.Button ("Speichern")) {
				audio.Play ();
				this.gameManager.SetPlayerName(this.playerName);
				this.gameManager.SaveSettings();
				this.menuState = MenuState.State_Main;
			}

			if (GUILayout.Button ("Zurücksetzen")) {
				audio.Play ();
				this.gameManager.LoadSettings();
				this.menuState = MenuState.State_Main;
			}

			if (GUILayout.Button ("Zurück")) {
				audio.Play ();
				this.menuState = MenuState.State_Main;			
			}

			GUILayout.EndVertical ();
		}

		/// <summary>
		/// Zeichnet das Credits Menü.
		/// </summary>
		private void DisplayCredits(){
			GUILayout.BeginVertical ();

			GUIStyle hls = new GUIStyle (GUI.skin.label);
			hls.fontSize = HEADING_LABEL_FONT_SIZE;
			hls.alignment = TextAnchor.MiddleCenter;
			GUILayout.Label("Credits", hls);

			GUILayout.FlexibleSpace ();

			GUIStyle ta = new GUIStyle(GUI.skin.textArea);
			ta.alignment = TextAnchor.MiddleCenter;
			GUILayout.TextArea (this.credits.text, ta);

			GUILayout.FlexibleSpace ();

			if (GUILayout.Button ("Zurück")) {
				audio.Play ();
				this.menuState = MenuState.State_Main;			
			}

			GUILayout.EndVertical ();
		}

		/// <summary>
		/// Beendet das Spiel
		/// </summary>
		IEnumerator Quit() {
			yield return new WaitForSeconds(0.3f);
			Application.Quit ();
		}

		/// <summary>
		/// Startet das Spiel.
		/// </summary>
		/// <returns>The game.</returns>
		IEnumerator StartGame() {

			yield return new WaitForSeconds(0.3f);
			this.gameManager.StartLevel();
		}
	}
}
