﻿using UnityEngine;
using System.Collections;
using Sabbac.MazeLogic;

namespace Sabbac {

	/// <summary>
	/// Verwaltet das OSD.
	/// </summary>
	public class OSDManager : MonoBehaviour {

		private static int SCREEN_BORDER_SPACING = 40;
		private static int OSD_FONT_SIZE = 24;

		/// <summary>
		/// Der Skin des Menüs.
		/// </summary>
		public GUISkin skin;

		/// <summary>
		/// Der zuständige GameManager.
		/// </summary>
		public GameManager gameManager;
		private bool osdEnabled = true;
		private Texture2D mapTextureWall;
		private Texture2D mapTextureNotVisited;
		private Texture2D mapTextureVisited;
		private Texture2D mapTextureCurrent;

		// Use this for initialization
		void Start () {
			this.gameManager = GameManager.Instance;

			this.mapTextureWall = new Texture2D (1, 1);
			this.mapTextureWall.SetPixel (0, 0, Color.black);
			this.mapTextureWall.Apply ();

			this.mapTextureNotVisited = new Texture2D (1, 1);
			this.mapTextureNotVisited.SetPixel (0, 0, Color.clear);
			this.mapTextureNotVisited.Apply ();

			this.mapTextureVisited = new Texture2D (1, 1);
			this.mapTextureVisited.SetPixel (0, 0, Color.grey);
			this.mapTextureVisited.Apply ();

			this.mapTextureCurrent = new Texture2D (1, 1);
			this.mapTextureCurrent.SetPixel (0, 0, Color.red);
			this.mapTextureCurrent.Apply ();
		}

		/// <summary>
		/// Schaltet den Zustand des OSDs zwischen AN und AUS um.
		/// </summary>
		public void ToggleOSD() {
			this.osdEnabled = !this.osdEnabled;
		}
			
		void OnGUI() {
			if (this.osdEnabled) {
				GUI.skin = this.skin;

				if (this.gameManager.GetDifficulty() != DifficultyLevel.Level_Hard) {
					DrawMazeMap ();
				}
				DrawTimerDisplay();
				DrawRemainingLivesDisplay();
			}
		}

		/// <summary>
		/// Zeichnet die Karte des Labyrinths.
		/// </summary>
		private void DrawMazeMap() {
			float h = Screen.height / 3;
			Maze m = this.gameManager.GetMaze ();
			int mazeHeight = m.GetHeight ();
			int mazeWidth = m.GetWidth ();

			GUI.BeginGroup (
				new Rect (
					Screen.width - h - SCREEN_BORDER_SPACING,
					h * 2 - SCREEN_BORDER_SPACING,
					h + (SCREEN_BORDER_SPACING / 2),
					h + (SCREEN_BORDER_SPACING / 2)
				)
			);

			GUI.Box(new Rect(0 ,0, h + (SCREEN_BORDER_SPACING / 2), h + (SCREEN_BORDER_SPACING / 2)), "");

			for (int x = 0; x < mazeHeight; x++) {
				for(int y = 0; y < mazeWidth; y++) {
					DrawMazeElement(x, y,new Rect(y * (h / mazeWidth) + 10,x * (h / mazeHeight) + 10, h / mazeWidth, h / mazeHeight));
				}
			}

			GUI.EndGroup ();
		}

		/// <summary>
		/// Zeichnet das einzelnes Labyrinthelement an den übergebenen Labyrinthkoordinaten.
		/// </summary>
		/// <param name="x">Die X Koordinate.</param>
		/// <param name="y">Die Y Koordinate..</param>
		/// <param name="r">Das Rechteck, in dem gezeichnet wird.</param>
		private void DrawMazeElement(int x, int y, Rect r) {
			Direction d = this.gameManager.GetMaze ().GetCell (x, y); //Direction
			Vector2 p = this.gameManager.GetPlayerPosMaze (); //Player
			Texture2D ft; //Floor texture

			if (!this.gameManager.HasBeenVisited (x, y)) {
				ft = this.mapTextureNotVisited;
				GUI.DrawTexture (r, ft);
			} else {
				if (p.x == x && p.y == y) {
					ft = this.mapTextureCurrent;		
				} else {
					ft = this.mapTextureVisited;
				}

				GUI.DrawTexture (r, ft);

				if ((d & Direction.N) != 0) {
					//GUI.DrawTexture (new Rect (r.x, r.y, r.width, r.height / 5), ft);
				} else {
					GUI.DrawTexture (new Rect (r.x, r.y, r.width, r.height / 5), this.mapTextureWall);	
				}

				if ((d & Direction.E) != 0) {
					//GUI.DrawTexture (new Rect (r.x + (r.width / 5) * 4, r.y + r.height / 5, r.width / 5, (r.height / 5) * 3), ft);		
				} else {
					GUI.DrawTexture (new Rect (r.x + (r.width / 5) * 4, r.y, r.width / 5, r.height), this.mapTextureWall);	
				}
				
				if ((d & Direction.S) != 0) {
					//GUI.DrawTexture (new Rect (r.x + r.width / 5, r.y + (r.height / 5) * 4, (r.width / 5) * 3, r.height / 5), ft);		
				} else {
					GUI.DrawTexture (new Rect(r.x, r.y + (r.height / 5) * 4, r.width, r.height / 5), this.mapTextureWall);	
				}
				
				if ((d & Direction.W) != 0) {
					//GUI.DrawTexture (new Rect (r.x, r.y + r.height / 5, r.width / 5, (r.height / 5) * 3), ft);		
				} else {
					GUI.DrawTexture (new Rect(r.x, r.y, r.width / 5, r.height), this.mapTextureWall);	
				}
			}
		}

		/// <summary>
		/// Zeichnet die Zeitanzeige.
		/// </summary>
		private void DrawTimerDisplay() {
			float t = this.gameManager.GetLevelTime (); //Elapsed time
			int minutes = (int)t / 60;
			int seconds = (int)t % 60;
			string text = string.Format ("{0:00}:{1:00}", minutes, seconds);
			GUIStyle style = new GUIStyle(GUI.skin.box);

			style.fontSize = OSD_FONT_SIZE; 

			GUILayout.BeginArea (
				new Rect (
					Screen.width - 250 - SCREEN_BORDER_SPACING,
					SCREEN_BORDER_SPACING,
					250,
					100
				)
			);
			
			GUILayout.Box (text, style);

			GUILayout.EndArea ();
		}

		/// <summary>
		/// Zeichnet die Anzeige für die verbleibenden Leben, des Spielers.
		/// </summary>
		private void DrawRemainingLivesDisplay() {

			string text = "Verbleibende Leben: " + this.gameManager.GetPlayer().GetRemainingLives();
			GUIStyle style = new GUIStyle(GUI.skin.box);
			
			style.fontSize = OSD_FONT_SIZE; 
			
			GUILayout.BeginArea (
				new Rect (
				Screen.width - 250 - SCREEN_BORDER_SPACING,
				SCREEN_BORDER_SPACING * 2,
				250,
				100
				)
				);
			
			GUILayout.Box (text, style);
			
			GUILayout.EndArea ();
		}
	}
}
