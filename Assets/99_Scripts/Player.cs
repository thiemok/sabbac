﻿using UnityEngine;
using System.Collections;

namespace Sabbac {

	/// <summary>
	/// Repräsentiert den Spieler.
	/// </summary>
	public class Player : MonoBehaviour {


		private static readonly int DIFFICULT_EASY_LIVES = 5;
		private static readonly int DIFFICULTY_NORMAL_LIVES = 3;
		private static readonly int DIFFICULTY_HARD_LIVES = 0;

		private static readonly int PRATICLES_EMITTED_PER_PULSE = 8;

		private static readonly float GROUND_DISTANCE_TOLERANCE = 0.3f;

		public float movementForce = 17;
		public float jumpForce = 15;
		public GameObject cameraRef;

		private float distToGround;
		private int lives;
		private Light _light;
		private float lightMinIntensity;
		private float lightMaxIntensity;
		private bool lightIntensityIncreasing = false;
		private Color defaultLightColor;
		private bool jumpLock = false;
		private bool respawning = false;

		void Awake() {
			this.distToGround = collider.bounds.extents.y;

			switch (GameManager.Instance.GetDifficulty()) {
			case DifficultyLevel.Level_Easy:
				this.lives = DIFFICULT_EASY_LIVES;
				break;

			case DifficultyLevel.Level_Normal:
				this.lives = DIFFICULTY_NORMAL_LIVES;
				break;

			case DifficultyLevel.Level_Hard:
				this.lives = DIFFICULTY_HARD_LIVES;
				break;
			}

			this._light = GetComponentInChildren<Light> ();
			this.lightMaxIntensity = this._light.intensity;
			this.lightMinIntensity = 1;
			this.defaultLightColor = this._light.color;

			StartCoroutine (DoLightAnimation ());
		}

		void OnDestroy() {
			StopAllCoroutines ();
		}

		void FixedUpdate() {

			if (IsGrounded ()) {
				float horizontal = Input.GetAxis ("Horizontal") * movementForce;
				float vertical = Input.GetAxis ("Vertical") * movementForce;

				Vector3 change = new Vector3 (horizontal, 0.0f, vertical);
				Quaternion rotation = Quaternion.Euler (0, cameraRef.transform.eulerAngles.y, 0);
				Vector3 movement = rotation * change;

				rigidbody.AddForce (movement, ForceMode.Force);

				float z = Input.GetAxis("Jump") * jumpForce;
				if(!jumpLock && z != 0) {
					Vector3 jump = new Vector3(0.0f, z, 0.0f);
					rigidbody.AddForce(jump, ForceMode.Impulse);
				}
			}

			if (Input.GetButtonUp ("Jump")) {
				this.jumpLock = false;
			} else if (Input.GetButtonDown ("Jump")) {
				this.jumpLock = true;
			}
		}

		void OnTriggerEnter(Collider c) {
			if (c.gameObject.tag == "PowerUp") {
				CollectPowerUp(c.gameObject);			
			}
		}

		public int GetRemainingLives() {
			return this.lives;
		}


		/// <summary>
		/// Lässt den Spieler ein Leben verlieren.
		/// </summary>
		/// <returns><c>true</c>, wenn der Spieler noch Leben übrig hat. <c>false</c>, wenn er keiner mehr übrig hat.</returns>
		public bool LooseLive() {
			bool lived = true;

			this.respawning = true;

			GetComponentInChildren<ParticleSystem> ().Emit(PRATICLES_EMITTED_PER_PULSE * 30);
			this._light.enabled = false;

			if(this.lives == 0) {
				lived = false;
			}

			if (this.lives > 0) {
				this.lives--;
			}

			return lived;
		}

		/// <summary>
		/// Fügt dem Spieler ein extra Leben hinzu.
		/// </summary>
		public void AddBonusLive() {
			this.lives++;		
		}

		/// <summary>
		/// Lässt den Spieler an dem übergebenen Punkt respawnen.
		/// </summary>
		/// <param name="respawnPoint">Der Respawnpunkt.</param>
		public void Respawn(Vector3 respawnPoint) {
			this.transform.position = respawnPoint + (new Vector3(0, this.distToGround * 4, 0));
			this.rigidbody.velocity = new Vector3 (0, 0, 0);
			this.respawning = false;
			StartCoroutine (DoRespawnAnimation ());
		}

		/// <summary>
		/// Zeigt an ob der Spieler gerade respawnt
		/// </summary>
		/// <returns><c>true</c>,wenn der Spieler gerade respawn, sonst <c>false</c>.</returns>
		public bool IsRespawning() {
			return respawning;		
		}

		/// <summary>
		/// Erhöht die Geschwindigkeit des Spielers für den übergebenen Zeitraum um den übergeben Faktor.
		/// </summary>
		/// <returns>Coroutine</returns>
		/// <param name="speedMultiplier">Der Geschwindigkeitsmultiplikator.</param>
		/// <param name="duration">Die Dauer.</param>
		public IEnumerator BoostSpeed(float speedMultiplier, float duration) {
			float remainingTime = duration;
			float defaultMovementForce = this.movementForce;
			ParticleSystem p = GetComponentInChildren<ParticleSystem> ();

			p.startColor = PowerUp.SPEED_BOOST_COLOR;
			this.movementForce = this.movementForce * speedMultiplier;
			this._light.color = PowerUp.SPEED_BOOST_COLOR;

			while (remainingTime > 0) {
				yield return null;
				remainingTime -= Time.deltaTime;
			}

			p.startColor = this.defaultLightColor;
			this._light.color = this.defaultLightColor;
			this.movementForce = defaultMovementForce;
		}

		/// <summary>
		/// Deaktiviert die Gravitation des Spielers für den gegebenen Zeitraum.
		/// </summary>
		/// <returns>Coroutine</returns>
		/// <param name="duration">Die Dauer.</param>
		public IEnumerator NoGravity(float duration) {
			float remainingTime = duration;
			ParticleSystem p = GetComponentInChildren<ParticleSystem> ();

			p.startColor = PowerUp.NO_GRAVITY_COLOR;
			this._light.color = PowerUp.NO_GRAVITY_COLOR;
			this.rigidbody.useGravity = false;

			while (remainingTime > 0) {
				yield return null;
				remainingTime -= Time.deltaTime;
			}

			p.startColor = this.defaultLightColor;
			this._light.color = this.defaultLightColor;
			this.rigidbody.useGravity = true;
		}

		/// <summary>
		/// Überprüft ob der Spieler sich auf dem Boden befindet.
		/// </summary>
		/// <returns><c>true</c>, wenn der Spieler sich innerhalb von <value>GROUND_DISTANCE_TOLERANCE</value> zum Boden befindet, sonst <c>false</c>.</returns>
		private bool IsGrounded() {
			bool grounded = true;

			if (this.rigidbody.useGravity) {
				Vector3 xOffset = new Vector3(collider.bounds.extents.x / 2, 0, 0);
				Vector3 pos = transform.position;
				grounded = Physics.Raycast(pos, -Vector3.up, distToGround + GROUND_DISTANCE_TOLERANCE);
				grounded |= Physics.Raycast(pos + xOffset, -Vector3.up, distToGround + GROUND_DISTANCE_TOLERANCE);
				grounded |= Physics.Raycast(pos - xOffset, -Vector3.up, distToGround + GROUND_DISTANCE_TOLERANCE);
			}

			return grounded;
		}

		/// <summary>
		/// Sammelt das übergebene PawerUp ein und aktiviert dessen Effekt.
		/// </summary>
		/// <param name="p">GameObject des PowerUps.</param>
		private void CollectPowerUp(GameObject p) {
			PowerUp powerUp = (PowerUp) p.GetComponent(typeof(PowerUp));

			GameManager.Instance.ApplyPowerUp (powerUp);

			Destroy (p);
		}

		/// <summary>
		/// Führt die Respawn Animation aus.
		/// </summary>
		/// <returns>Coroutine</returns>
		IEnumerator DoRespawnAnimation() {
			this._light.enabled = true;
			ParticleSystem p = GetComponentInChildren<ParticleSystem> ();
			p.Clear ();
			//p.Pause ();
			for (int i = 0; i < 6; i++) {
				yield return new WaitForSeconds(0.3f);
				//this.renderer.enabled = !this.renderer.enabled;
				this._light.enabled = !this._light.enabled;
				foreach(Renderer r in GetComponentsInChildren<Renderer>()) {
					r.enabled = !r.enabled;
				}
			}
			//p.Play ();
		}

		/// <summary>
		/// Führt die Animation des Lichts im Spielermodell aus.
		/// </summary>
		/// <returns>Coroutine</returns>
		IEnumerator DoLightAnimation() {
			while (true) {
				float rand = UnityEngine.Random.value * 0.1f * Time.timeScale;
				if(!this.lightIntensityIncreasing) {
					rand = -rand;
				}

				this._light.intensity += rand;

				if(this._light.intensity > this.lightMaxIntensity) {
					this.lightIntensityIncreasing = false;
					GetComponentInChildren<ParticleSystem> ().Emit(PRATICLES_EMITTED_PER_PULSE);
				} else if(this._light.intensity < this.lightMinIntensity) {
					this.lightIntensityIncreasing = true;
				}

				yield return null;
			}
		}
	}
}
