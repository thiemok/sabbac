﻿using UnityEngine;
using System.Collections;

namespace Sabbac {

	/// <summary>
	/// Verwaltet das Spielmenü.
	/// </summary>
	public class GameMenuManager : MonoBehaviour {

		private enum MenuState {
			State_Disabled,
			State_Pause_Menu,
			State_Won_Menu,
			State_Lost_Menu,
			State_Save_Level_Menu
		}

		/// <summary>
		/// Der verwendete Skin.
		/// </summary>
		public GUISkin skin;
		/// <summary>
		/// Die AudioSource für die Menü Sounds.
		/// </summary>
		public AudioSource menuSoundSource;

		private GameManager gameManager;
		private int screenDivider = 8;
		private int menuPosMultiplier;
		private MenuState menuState;
		private MenuState previousMenuState;
		private bool isHighscore = false;
		private int loadGameIndex;
		private string[] savedGameTitles;
		private string newSaveTitle= "";
		private string currentGameCode;
		private bool guiInitialized = false;

		// Use this for initialization
		void Start () {
			this.gameManager = GameManager.Instance;
			this.menuState = MenuState.State_Disabled;
			this.menuPosMultiplier = this.screenDivider / 6; //Do not change
		}
		
		// Update is called once per frame
		void Update () {
			if (Input.GetKeyDown (KeyCode.P)) {
				if (this.gameManager.LevelIsRunning()) {
					this.gameManager.TogglePause();
				}
			}
		}

		/// <summary>
		/// Zeichnet das Menügerüst und wechselt zwischen den einzelnen Inhalten.
		/// </summary>
		void OnGUI(){
			if (this.menuState != MenuState.State_Disabled) {

				if (!this.guiInitialized) {
					this.initGUI();
				}

				GUI.skin = this.skin;
			
				GUILayout.BeginArea (
					new Rect (
						(Screen.width / this.screenDivider) * this.menuPosMultiplier,
						(Screen.height / this.screenDivider) * this.menuPosMultiplier,
						(Screen.width / this.screenDivider) * (this.screenDivider - 2 * this.menuPosMultiplier),
						(Screen.height / this.screenDivider) * (this.screenDivider - 2 * this.menuPosMultiplier)
					),
					GUI.skin.box
				);
			
				GUILayout.BeginVertical ();
			
				switch(this.menuState) {

				case MenuState.State_Won_Menu:
					DrawWonMenu();
					break;

				case MenuState.State_Lost_Menu:
					DrawLostMenu();
					break;

				case MenuState.State_Pause_Menu:
					DrawPauseMenu();
					break;

				case MenuState.State_Save_Level_Menu:
					DrawSaveLevelMenu();
					break;

				default:
					break;
				}

				GUILayout.FlexibleSpace();

				if (GUILayout.Button ("Zurück zum Hauptmenü")) {
					this.menuSoundSource.Play();
					this.DisableMenu();
					Time.timeScale = 1;
					StartCoroutine(HelperFunctions.DoAfterWait(0.3f, delegate() {
						this.gameManager.LoadMainMenu();
					}));
				}

				GUILayout.EndVertical ();

				GUILayout.EndArea ();
			} else {
				if (this.guiInitialized) {
					resetGUI();
				}
			}
		}

		/// <summary>
		/// Zeigt das Menü an, das nach dem Sieg des Spielers gezeigt wird.
		/// </summary>
		/// <param name="highscore">Zeigt an, ob der Spieler einen neuen Highscore geschafft hat.</param>
		public void DisplayWonMenu(bool highscore) {
			this.isHighscore = highscore;
			this.menuState = MenuState.State_Won_Menu;
		}

		/// <summary>
		/// Zeigt das Menü an, das angezeigt wird, nachdem der Spieler das Spiel verloren hat.
		/// </summary>
		public void DisplayLostMenu() {
			this.menuState = MenuState.State_Lost_Menu;
		}

		/// <summary>
		/// Zeigt das Pause Menü an.
		/// </summary>
		public void DisplayPauseMenu() {
			this.menuState = MenuState.State_Pause_Menu;
		}

		/// <summary>
		/// Deaktiviert das Menü.
		/// </summary>
		public void DisableMenu() {
			this.menuState = MenuState.State_Disabled;		
		}

		/// <summary>
		/// Zeichnet das Menü, das nach dem Sieg des Spielers gezeigt wird.
		/// </summary>
		private void DrawWonMenu() {
			GUILayout.BeginVertical ();

			Color guiColor = GUI.contentColor;
			GUI.contentColor = Color.green;
			GUIStyle hls = new GUIStyle (GUI.skin.label);
			hls.fontSize = MainMenuManager.TITLE_LABEL_FONT_SIZE;
			hls.alignment = TextAnchor.MiddleCenter;
			GUILayout.Label ("Gewonnen!", hls);
			GUI.contentColor = guiColor;

			GUIStyle tls = new GUIStyle (hls);
			tls.fontSize = MainMenuManager.HEADING_LABEL_FONT_SIZE;
			float t = this.gameManager.GetLevelTime (); //Elapsed time
			int minutes = (int)t / 60;
			int seconds = (int)t % 60;
			string text = string.Format ("{0:00}:{1:00}", minutes, seconds);
			GUILayout.Label (text, tls);

			GUILayout.FlexibleSpace ();

			GUIStyle ls = new GUIStyle(GUI.skin.label);
			ls.alignment = TextAnchor.MiddleCenter;
			ls.fontSize = MainMenuManager.OPTION_HEADING_LABEL_FONT_SIZE;

			if (this.isHighscore) {
				GUILayout.Label("Neuer Highscore!", ls);			
			}

			GUILayout.Label ("Highscores", ls);
			string scoreboard = "\n";
			Highscores.Score[] h = this.gameManager.GetHighscores ().GetScores();
			foreach (Highscores.Score s in h) {
				t = s.time;
				minutes = (int)t / 60;
				seconds = (int)t % 60;
				scoreboard += s.name + " - " + 	string.Format ("{0:00}:{1:00}", minutes, seconds) + "\n\n";		
			}
			GUILayout.TextArea (scoreboard);

			GUILayout.FlexibleSpace ();

			if (GUILayout.Button ("Level Speichern")) {
				this.menuSoundSource.Play();
				this.previousMenuState = this.menuState;
				this.menuState = MenuState.State_Save_Level_Menu;
			}

			if (GUILayout.Button ("Level neu starten")) {
				this.menuSoundSource.Play();
				this.gameManager.RestartLevel();
			}

			GUILayout.EndVertical();
		}

		/**
		 * Draws the menu that is shown after the player has lost the game.
		 */
		private void DrawLostMenu() {
			GUILayout.BeginVertical ();

			Color guiColor = GUI.contentColor;
			GUI.contentColor = Color.red;
			GUIStyle hls = new GUIStyle (GUI.skin.label);
			hls.fontSize = MainMenuManager.TITLE_LABEL_FONT_SIZE;
			hls.alignment = TextAnchor.MiddleCenter;
			GUILayout.Label ("Verloren!", hls);
			GUI.contentColor = guiColor;

			GUILayout.FlexibleSpace ();

			if (GUILayout.Button ("Level Speichern")) {
				this.menuSoundSource.Play();
				this.previousMenuState = this.menuState;
				this.menuState = MenuState.State_Save_Level_Menu;
			}
			
			if (GUILayout.Button ("Level neu starten")) {
				this.menuSoundSource.Play();
				this.gameManager.RestartLevel();
			}

			GUILayout.EndVertical ();
		}

		/// <summary>
		/// Zeichnet das Menü, das angezeigt wird, nachdem der Spieler das Spiel verloren hat.
		/// </summary>
		private void DrawPauseMenu() {
			GUILayout.BeginVertical ();

			GUIStyle hls = new GUIStyle (GUI.skin.label);
			hls.fontSize = MainMenuManager.TITLE_LABEL_FONT_SIZE;
			hls.alignment = TextAnchor.MiddleCenter;
			GUILayout.Label ("Pause", hls);
			
			GUILayout.FlexibleSpace ();

			float t = this.gameManager.GetLevelTime (); //Elapsed time
			int minutes = (int)t / 60;
			int seconds = (int)t % 60;
			string text = string.Format ("{0:00}:{1:00}", minutes, seconds);
			GUILayout.Label (text, hls);
			
			GUILayout.FlexibleSpace ();

			if (GUILayout.Button ("Spiel fortsetzen")) {
				this.menuSoundSource.Play();
				this.gameManager.ResumeGame();
			}

			if (GUILayout.Button ("Level Speichern")) {
				this.menuSoundSource.Play();
				this.previousMenuState = this.menuState;
				this.menuState = MenuState.State_Save_Level_Menu;
			}
			
			if (GUILayout.Button ("Level neu starten")) {
				this.menuSoundSource.Play();
				this.gameManager.RestartLevel();
			}
			
			GUILayout.EndVertical ();
		}

		/// <summary>
		/// Zeichnet das Spiel speichern Menü.
		/// </summary>
		private void DrawSaveLevelMenu() {
			GUILayout.BeginVertical ();

			GUIStyle hls = new GUIStyle (GUI.skin.label);
			hls.fontSize = MainMenuManager.TITLE_LABEL_FONT_SIZE;
			hls.alignment = TextAnchor.MiddleCenter;
			GUILayout.Label ("Level speichern", hls);

			GUILayout.FlexibleSpace ();

			hls.fontSize = MainMenuManager.OPTION_HEADING_LABEL_FONT_SIZE;
			GUILayout.Label ("Speicherstände", hls);

			this.savedGameTitles = this.gameManager.GetSavedGameTitles ();
			this.savedGameTitles[this.savedGameTitles.Length - 1] = "Level Teilen";
			this.loadGameIndex = GUILayout.SelectionGrid (this.loadGameIndex, this.gameManager.GetSavedGameTitles(), 1);

			GUILayout.FlexibleSpace ();

			if (this.loadGameIndex == this.gameManager.GetSavedGameTitles ().Length - 1) {
				GUILayout.Label ("Verwende diesen Code um dein Spiel zu Teilen", hls);
				GUILayout.TextField (this.currentGameCode);
			} else {
				GUILayout.Label ("Titel des Speicherstands", hls);
				this.newSaveTitle= GUILayout.TextField(this.newSaveTitle);	
			}

			GUILayout.FlexibleSpace ();

			if (GUILayout.Button ("Level Speichern")) {
				this.menuSoundSource.Play();
				this.gameManager.SaveGame(this.newSaveTitle, this.loadGameIndex);
				this.menuState = this.previousMenuState;
			}

			if (GUILayout.Button ("Zurück")) {
				this.menuSoundSource.Play();
				this.menuState = this.previousMenuState;
			}

			GUILayout.EndVertical ();
		}

		/// <summary>
		/// Setzt das GUI zurück.
		/// </summary>
		private void resetGUI() {
			this.currentGameCode = "";
			this.guiInitialized = false;
		}

		/// <summary>
		/// Initialisiert das GUI.
		/// </summary>
		private void initGUI() {
			this.currentGameCode = this.gameManager.SerializeGame ();	
			this.guiInitialized = true;
		}
	}
}
