﻿using UnityEngine;
using System.Collections.Generic;
using Sabbac.MazeLogic;

namespace Sabbac {

	/// <summary>
	/// Erzeugt das Level.
	/// </summary>
	public class LevelBuilder : MonoBehaviour {

		/// <summary>
		/// Levelbausteine für Sackgassen.
		/// </summary>
		public GameObject[] deadEnds;
		/// <summary>
		/// Levelbausteine für Kammern.
		/// </summary>
		public GameObject[] chambers;
		/// <summary>
		/// Levelbausteine für T-Kreuzungen.
		/// </summary>
		public GameObject[] tJunctions;
		/// <summary>
		/// Levelbausteine für Kurven.
		/// </summary>
		public GameObject[] corners;
		/// <summary>
		/// Levelbaustein für das Startfeld.
		/// </summary>
		public GameObject start;
		/// <summary>
		/// Levelbaustein für das Zielfeld.
		/// </summary>
		public GameObject finish;
		/// <summary>
		/// GameObject des PowerUps.
		/// </summary>
		public GameObject powerUp;
		/// <summary>
		/// Breite eines Levelbausteins.
		/// </summary>
		public int tileWidth = 30;
		/// <summary>
		/// Höhe eines Levelnausteins.
		/// </summary>
		public int tileHeight = 30;
		/// <summary>
		/// GameObject des Spielers.
		/// </summary>
		public GameObject player;
		/// <summary>
		/// GameObject der Hauptkamera.
		/// </summary>
		public GameObject playerCamera;

		private Maze maze;
		private GameManager gameManager;
		//Tile types
		private enum Tile {Corner, TJunction, Chamber, DeadEnd, Finish}; 

		//Tile decks for random placement
		private Deck<GameObject> deadEndDeck;
		private Deck<GameObject> chamberDeck;
		private Deck<GameObject> tJunctionDeck;
		private Deck<GameObject> cornerDeck;

		//Tile associations
		private Dictionary<Direction, Tile> tileAssociations = new Dictionary<Direction, Tile>{
			{Direction.N, Tile.DeadEnd},
			{Direction.E, Tile.DeadEnd},
			{Direction.S, Tile.DeadEnd},
			{Direction.W, Tile.DeadEnd},
			{(Direction.N | Direction.E), Tile.Corner},
			{(Direction.E | Direction.S), Tile.Corner},
			{(Direction.S | Direction.W), Tile.Corner},
			{(Direction.W | Direction.N), Tile.Corner},
			{(Direction.N | Direction.S), Tile.Chamber},
			{(Direction.E | Direction.W), Tile.Chamber},
			{(Direction.N | Direction.E | Direction.S), Tile.TJunction},
			{(Direction.E | Direction.S | Direction.W), Tile.TJunction},
			{(Direction.S | Direction.W | Direction.N), Tile.TJunction},
			{(Direction.W | Direction.N | Direction.E), Tile.TJunction}
		};

		//Rotations
		private Dictionary<Direction, int> rotationAssociations = new Dictionary<Direction, int>{
			{Direction.N, 180},
			{Direction.E, -90},
			{Direction.S, 0},
			{Direction.W, 90},
			{(Direction.N | Direction.E), 90},
			{(Direction.E | Direction.S), 180},
			{(Direction.S | Direction.W), -90},
			{(Direction.W | Direction.N), 0},
			{(Direction.N | Direction.S), 0},
			{(Direction.E | Direction.W), 90},
			{(Direction.N | Direction.E | Direction.S), 180},
			{(Direction.E | Direction.S | Direction.W), -90},
			{(Direction.S | Direction.W | Direction.N), 0},
			{(Direction.W | Direction.N | Direction.E), 90}
		};

		void Awake() {
			this.gameManager = GameManager.Instance;

			/* Init Decks */
			this.deadEndDeck = new Deck<GameObject>(this.deadEnds);
			this.chamberDeck = new Deck<GameObject> (this.chambers);
			this.tJunctionDeck = new Deck<GameObject> (this.tJunctions);
			this.cornerDeck = new Deck<GameObject> (this.corners);
		}

		/// <summary>
		/// Liefert das verwendete Labyrinth.
		/// </summary>
		/// <returns>The maze.</returns>
		public Maze GetMaze() {
			return this.maze;
		}

		/// <summary>
		/// Erzeugt das Level mit dem übergebenen Labyrinth.
		/// </summary>
		/// <param name="m">Das Labyrinth.</param>
		public void BuildLevelWithMaze(Maze m) {
			this.maze = m;
			BuildLevel ();
		}

		/// <summary>
		/// Erzeugt das eigentliche Level.
		/// </summary>
		private void BuildLevel() {
			Vector3 pos;
			TileManager tm;

			//Place start tile
			pos = new Vector3 (tileHeight * -1, 0, 0);
			GameObject t = (GameObject)GameObject.Instantiate (start, pos, Quaternion.identity);
			tm = (TileManager)t.AddComponent("StartTileManager");
			tm.gameManager = this.gameManager;
			tm.mazePosX = -1;
			tm.mazePosY = 0;

			//Move Player and Camera
			player.transform.position += pos;
			playerCamera.transform.position += pos;

			//Place maze tiles
			for (int row = 0; row < maze.GetHeight(); row++) {
				for(int col = 0; col < maze.GetWidth(); col++) {
					pos = new Vector3(tileHeight * row, 0, tileWidth * col);
					Quaternion rotation = Quaternion.identity;
					Tile tile = ParseTile(row, col, ref rotation);

					t = (GameObject)GameObject.Instantiate(ChooseTile(tile), pos, rotation);

					if(tile == Tile.Finish) {
						tm = (TileManager)t.AddComponent("FinishTileManager");
					} else {
						tm = (TileManager)t.AddComponent("TileManager");
					}

					if(tile == Tile.DeadEnd) {
						Vector3 p = t.transform.FindChild("RespawnPoint").position;
						GameObject.Instantiate(this.powerUp, p, Quaternion.identity);
					}

					tm.gameManager = this.gameManager;
					tm.mazePosX = row;
					tm.mazePosY = col;
				}
			}
		}

		/**
		 * Parses the actual used tile at the given position and its rotation from the maze data.
		 * The rotation is provided via the referenced quaternion
		 */
		/// <summary>
		/// Liest das eigentliche verwendete Feldtyp und dessen Rotation an der übergebenen Position aus den Labyrinthdaten.
		/// Die Rotation wird über das referenzierte Quaternion geliefert.
		/// </summary>
		/// <returns>Das Feld.</returns>
		/// <param name="x">Die X Koordinate.</param>
		/// <param name="y">Die Y Koordinate.</param>
		/// <param name="r">Die Referenz auf die Rotation.</param>
		private Tile ParseTile(int x, int y, ref Quaternion r) {
			Direction d = maze.GetCell (x, y);
			Tile t;

			if ((x == maze.GetFinishX()) && (y == maze.GetFinishY())) {
				t = Tile.Finish;		
			} else {
				t = tileAssociations[d];		
			}

			r = Quaternion.Euler (0, rotationAssociations [d], 0);

			return t;
		}

		/// <summary>
		/// Wähl ein zufälliges Feld für den übergebenen Feldtypen.
		/// </summary>
		/// <returns>Das Feld.</returns>
		/// <param name="t">Der Feldtyp.</param>
		private GameObject ChooseTile(Tile t) {
			GameObject tile = null;

			switch (t) {
			case Tile.Chamber:
				tile = chamberDeck.DrawCard();
				break;

			case Tile.Corner:
				tile = cornerDeck.DrawCard();
				break;
			
			case Tile.DeadEnd:
				tile = deadEndDeck.DrawCard();
				break;

			case Tile.TJunction:
				tile = tJunctionDeck.DrawCard();
				break;

			case Tile.Finish:
				tile = finish;
				break;
			}
			return tile;
		}
	}
}
