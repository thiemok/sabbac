using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Sabbac {
	namespace MazeLogic
	{
		/// <summary>
		/// Repräsentiert die Richtungen aus denen ein Labyrinthelement besucht werden kann.
		/// </summary>
		[FlagsAttribute()]
		public enum Direction {
			N = 1,
			S = 2,
			E = 4,
			W = 8
		}

		/// <summary>
		/// Repräsentiert ein Labyrinth.
		/// </summary>
		[Serializable]
		public struct Maze {
			[SerializeField] int width;
			[SerializeField] int height;
			[SerializeField] int[,] cells;
			[SerializeField] int finishX;
			[SerializeField] int finishY;

			int longestPath;

			/// <summary>
			/// Erzeugt ein Labyrinth und initialisiert es mit den übergebenen Maßen.
			/// </summary>
			/// <param name="width">Die Breite des Labyrinths.</param>
			/// <param name="heigth">Die Höhe des Labyrinths.</param>
			public Maze(int width, int heigth) {
				this.width = width;
				this.height = heigth;
				this.longestPath = 0;
				this.finishX = 0;
				this.finishY = 0;

				this.cells = new int[height, width];

				for(int i = 0; i < heigth; i++) {
					for(int j = 0; j < width; j++) {
						cells[i, j] = 0;
					}
				}
			}

			/// <summary>
			/// Liefert die Breite das Labyrinths.
			/// </summary>
			/// <returns>Die Breite.</returns>
			public int GetWidth () {
				return this.width;
			}

			/// <summary>
			/// Liefert die Höhe des Labyrinths.
			/// </summary>
			/// <returns>Die Höhe.</returns>
			public int GetHeight() {
				return this.height;
			}

			/// <summary>
			/// Liefert die Zelle an den übergebenen Koordinaten, wenn diese sich innerhalb der Grenzen des Labyrinths befinden.
			/// </summary>
			/// <returns>Die Zelle, wenn diese sich innerhalb der Grenzen befindet, sonst 0.</returns>
			/// <param name="x">Die X Koordinate.</param>
			/// <param name="y">Die Y Koordinate.</param>
			public Direction GetCell(int x, int y) {
				Direction d = 0;
				if (IsInBounds (x, y)) {
					d = (Direction)this.cells[x,y];
				}
				return d;
			}

			/// <summary>
			/// Liefert die X Koordinate des Ziels.
			/// </summary>
			/// <returns>Die X Koordinate des Ziels.</returns>
			public int GetFinishX() {
				return this.finishX;		
			}

			/// <summary>
			/// Liefert die Y Koordinate des Ziels.
			/// </summary>
			/// <returns>Die Y Koordinate des Ziels.</returns>
			public int GetFinishY() {
				return this.finishY;		
			}

			/// <summary>
			/// Liefert die Länge des längsten Pfads.
			/// </summary>
			/// <returns>Die länge des längsten Pfads.</returns>
			internal int GetLongestPathLength() {
				return this.longestPath;
			}

			/// <summary>
			/// Updatet den längsten Pfad.
			/// </summary>
			/// <param name="finishX">X Koordinate des Endpunkts.</param>
			/// <param name="finishY">Y koordinate des Endpunkts.</param>
			/// <param name="pathLength">Länge des Pfads.</param>
			internal void UpdateLongestPath(int finishX, int finishY, int pathLength) {
				if (pathLength > longestPath) {
					this.longestPath = pathLength;
					this.finishX = finishX;
					this.finishY = finishY;
				}
			}

			/// <summary>
			/// Überprüft ob sich die übergebene Position innerhalb der Grenzen des Labyrinths befindet.
			/// </summary>
			/// <returns><c>true</c>, wenn X und Y innerhalb der Grenzen ist, sonst <c>false</c>.</returns>
			/// <param name="x">Die X Koordinate.</param>
			/// <param name="y">Die Y Koordinate.</param>
			public bool IsInBounds(int x, int y) {
				bool inBounds = false;
				
				if (x < this.height && x >= 0) {
					if(y < this.width && y >= 0) {
						inBounds = true;
					}
				}
					
				return inBounds;
			}

			/// <summary>
			/// Überprüft ob die Zelle an der übergebenen Position bereits besucht wurde.
			/// </summary>
			/// <returns><c>true</c>, wenn die Zelle breits besucht wurde, sonst <c>false</c>.</returns>
			/// <param name="x">Die X Koordinate.</param>
			/// <param name="y">Die Y Koordinate.</param>
			public bool HasBeenVisited(int x, int y) {
				bool visited = true;

				if (this.cells [x, y] == 0) {
					visited = false;
				}

				return visited;
			}

			/// <summary>
			/// Besucht die Zelle an den übergebenen Koordinaten. Aus der übergebenen Richtung.
			/// </summary>
			/// <param name="x">Die X Koordinate.</param>
			/// <param name="y">Die Y Koordinate.</param>
			/// <param name="d">Die Richtung aus der die Zelle besucht wird.</param>
			public void Visit(int x, int y, Direction d) {
				this.cells [x, y] |= (int)d;
			}
		}

		/// <summary>
		/// Erzeugt Labyrinthe mithilfe des recursive backtracking Algorithmus.
		/// </summary>
		public static class MazeGenerator {

			/// <summary>
			/// Erzeugt ein Labyrinth mit den übergebenen Dimension mithilfe des recursive backtracking Algorithmus.
			/// </summary>
			/// <returns>Das erzeugte Labyrinth.</returns>
			/// <param name="width">Die Breite des zu erzeugenen Labyrinths.</param>
			/// <param name="height">Die Höhe des zu erzeugenen Labyrinths.</param>
			public static Maze GenerateMaze(int width, int height) {
				Maze maze = new Maze (width, height);

				CarvePassagesFrom (0, 0, ref maze, 0);

				//Insert passage for starting point
				maze.Visit (0, 0, Direction.N);

				return maze;
			}

			/// <summary>
			/// Eigentliche Implementierung des recursive backtracking Algorithmus.
			/// </summary>
			/// <param name="currentX">Aktuelle X Koordinate.</param>
			/// <param name="currentY">Aktuelle Y Koordinate</param>
			/// <param name="maze">Verwendetes Labyrinth.</param>
			/// <param name="pathLength">Länge des aktuellen Pfads.</param>
			private static void CarvePassagesFrom(int currentX, int currentY, ref Maze maze, int pathLength) {

				Direction[] OPPOSITE = new Direction[9];
				int[] DX = new int[9];
				int[] DY = new int[9];

				OPPOSITE [(int)Direction.N] = Direction.S;
				OPPOSITE [(int)Direction.E] = Direction.W;
				OPPOSITE [(int)Direction.S] = Direction.N;
				OPPOSITE [(int)Direction.W] = Direction.E;

				DY [(int)Direction.N] = 0;
				DY [(int)Direction.E] = 1;
				DY [(int)Direction.S] = 0;
				DY [(int)Direction.W] = -1;

				DX [(int)Direction.N] = -1;
				DX [(int)Direction.E] = 0;
				DX [(int)Direction.S] = 1;
				DX [(int)Direction.W] = 0;

				//Create list of all directions and put them in a random order
				var directions = new List<Direction>{
					Direction.N,
					Direction.S,
					Direction.E,
					Direction.W
				}.OrderBy(x => Guid.NewGuid());

				//Check if adjacent cells have been visited and if not create passages to them and continue with them
				foreach (Direction d in directions) {

					int nextX = currentX + DX[(int)d];
					int nextY = currentY + DY[(int)d];

					if(maze.IsInBounds(nextX, nextY)) {
						if(!maze.HasBeenVisited(nextX, nextY)) {
							maze.Visit(currentX, currentY, d);
							maze.Visit(nextX, nextY, OPPOSITE[(int)d]);

							CarvePassagesFrom(nextX, nextY, ref maze, pathLength + 1);
						} else {
							if(pathLength > maze.GetLongestPathLength()) {
								maze.UpdateLongestPath(currentX, currentY, pathLength);
							}
						}
					}
				}
			}
		}
	}
}

