﻿using UnityEngine;
using System.Collections.Generic;

namespace Sabbac {

	/// <summary>
	/// Repräsentiert ein generisches Kartendeck, das sich nach ziehen aller Karten selbst neu mischt.
	/// </summary>
	public class Deck<T> {

		/// <summary>
		/// Die verfügbaren Karten im Deck.
		/// </summary>
		private T[] availableCards;

		/// <summary>
		/// Der aktuelle Kartenstapel.
		/// </summary>
		private Queue<T> currentStack;

		/// <summary>
		/// Erzeugt ein Deck aus den übergebenen Karten. 
		/// </summary>
		/// <param name="cards">Die Karten.</param>
		public Deck(T[] cards) {
			this.availableCards = cards;
			ShuffleDeck ();
		}

		/// <summary>
		/// Zieht eine Karte vom Stapel und mischt die Karten neu, falls der Stapel leer wird.
		/// </summary>
		/// <returns>Die gezogene Karte.</returns>
		public T DrawCard() {
			if (currentStack.Count == 0) {
				ShuffleDeck();			
			}
			return currentStack.Dequeue ();
		}

		/// <summary>
		/// Mischt die verfügbaren Karten und platziert sie auf dem Stapel.
		/// </summary>
		private void ShuffleDeck() {
			T[] tmpStack = availableCards;
			System.Random _random = new System.Random ();
			int n = tmpStack.Length;

			for (int i = 0; i < n; i++)
			{
				int r = i + (int)(_random.NextDouble() * (n - i));
				T tmp = tmpStack[r];
				tmpStack[r] = tmpStack[i];
				tmpStack[i] = tmp;
			}
			currentStack = new Queue<T>(tmpStack);
		}
	}
}
