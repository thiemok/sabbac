using UnityEngine;
using System.Collections;

public class Test_LevelBuilder : MonoBehaviour {

	public GameObject[] tiles;
	public int tileWidth = 30;
	public int tileHeight = 30;

	// Use this for initialization
	void Start () {

		Vector3 pos;


		for (int i = 0; i < 3; i++) {

			pos = new Vector3(tileWidth * i, 0, 0);
			GameObject.Instantiate(tiles[0], pos, Quaternion.identity);

		}
	}

}
