﻿using UnityEngine;
using System.Collections;

namespace Sabbac {

	/// <summary>
	/// Repräsentiert den Timer, der die Spielzeit misst.
	/// </summary>
	public class LevelTimer : MonoBehaviour {

		private float elapsed;
		private float lastTimestamp;
		private bool paused;

		/// <summary>
		/// Startet den Timer.
		/// </summary>
		public void StartTimer () {
			ResetTimer ();
			this.lastTimestamp = Time.time;
			this.paused = false;
		}

		/// <summary>
		/// Pausuert den Timer, oder setzt ihn fort.
		/// </summary>
		public void TogglePause() {
			this.paused = !this.paused;
		}

		/// <summary>
		/// Setzt den Timer zurück.
		/// </summary>
		public void ResetTimer() {
			this.paused = true;
			this.elapsed = 0.0f;
		}

		/// <summary>
		/// Liefert die abgelaufene Zeit.
		/// </summary>
		/// <returns>Die abgelaufene Zeit.</returns>
		public float GetElapsedTime() {
			return this.elapsed;
		}

		/// <summary>
		/// Reduziert die abgelaufene Zeit um den übergebenen Wert in Sekunden.
		/// </summary>
		/// <param name="t">Sekunden um die die Zeit reduziert werden soll.</param>
		public void ReduceElapsedTime(float t) {
			this.elapsed -= t;	
		}

		// Update is called once per frame
		void Update () {
			if(!paused){
				float currentTime = Time.time;
				this.elapsed += currentTime - this.lastTimestamp;
				this.lastTimestamp = currentTime;
			}
		}
	}
}