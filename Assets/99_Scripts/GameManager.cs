﻿using UnityEngine;
using System.Collections;
using Sabbac.MazeLogic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Sabbac {
	
	/// <summary>
	/// Die verfügbaren Spielmodi.
	/// </summary>
	public enum GameMode {
		MODE_RANDOM_MAZE,
		MODE_LOAD_MAZE
	}
	
	/// <summary>
	/// Die verfügbaren Schwierigkeitsgrade.
	/// </summary>
	public enum DifficultyLevel {
		Level_Easy,
		Level_Normal,
		Level_Hard
	}

	/// <summary>
	/// Die verfügbaren Labyrinthgrößen.
	/// </summary>
	public enum MazeSize {
		Size_Small,
		Size_Medium,
		Size_Large
	}
	
	/// <summary>
	/// Repräsentiert ein gespeichertes Level.
	/// </summary>
	[Serializable]
	public struct Save {
		/// <summary>
		/// Der Name des Speicherstands.
		/// </summary>
		public string name;
		/// <summary>
		/// Die gespeicherten Spieldaten. Base64 encodiert.
		/// </summary>
		public string data;

		/// <summary>
		/// Initialisiert ein neues Object der <see cref="Sabbac.Save"/> Struktur.
		/// </summary>
		/// <param name="n">Der Name.</param>
		/// <param name="d">Die Spieldaten, Base64 encodiert.</param>
		public Save(string n, string d) {
			name = n;
			data = d;
		}
	}

	/// <summary>
	/// Repräsentiert den Spielmanager.
	/// Verantwortlich für die verknüpfung der einzelnen Elemente.
	/// </summary>
	public class GameManager : Singleton<GameManager> {

		/// <summary>
		/// Prefix für die Speicherung von Spieldaten in den Playerprefs.
		/// </summary>
		public readonly static string PREFS_GAME_PREFIX = "SABBAC";
		/// <summary>
		/// Speicherort der gewählten Schwierigkeit in den PLayerprefs.
		/// </summary>
		public readonly static string PREFS_DIFFICULTY_DESTINATION = PREFS_GAME_PREFIX + "_" + "DIFFICULTY";
		/// <summary>
		/// Speicherort der gewählten Labyrinthgröße in den Playerprefs.
		/// </summary>
		public readonly static string PREFS_MAZE_SIZE_DESTINATION = PREFS_GAME_PREFIX + "_" + "MAZE_SIZE";
		/// <summary>
		/// Speicherort des gewählten Spielmodus in den Playerprefs.
		/// </summary>
		public readonly static string PREFS_GAME_MODE_DESTINATION = PREFS_GAME_PREFIX + "_" + "GAME_MODE";
		/// <summary>
		/// Speicherort der gewählten Musiklautstärke in den Playerprefs.
		/// </summary>
		public readonly static string PREFS_MUSIC_VOLUME_DESTINATION = PREFS_GAME_PREFIX + "_" + "MUSIK_VOLUME";
		/// <summary>
		/// Speicherort der gewählten allgemeinen Lautstärke in den Playerprefs.
		/// </summary>
		public readonly static string PREFS_VOLUME_DESTINATION = PREFS_GAME_PREFIX + "_" + "VOLUME";
		/// <summary>
		/// Speicherort der gewählten Auflösung in den Playerprefs.
		/// </summary>
		public readonly static string PREFS_RESOLUTION_CHOICE_DESTINATION = PREFS_GAME_PREFIX + "_" + "RESOLUTION";
		/// <summary>
		/// Speicherort der Speicherstände in den Playerprefs.
		/// </summary>
		public readonly static string PREFS_SAVES_DESTINATION = PREFS_GAME_PREFIX + "_" + "SAVED_GAMES";
		/// <summary>
		/// Speicherort des zu ladenen Spielstands in den Playerprefs.
		/// </summary>
		public readonly static string PREFS_GAME_TO_LOAD_DESTINATION = PREFS_GAME_PREFIX + "_" + "GAME_TO_LOAD";
		/// <summary>
		/// Speicherort des Spielernamens in den Playerprefs.
		/// </summary>
		public readonly static string PREFS_PLAYER_NAME_DESTINATION = PREFS_GAME_PREFIX + "_" + "PLAYER_NAME";

		private static readonly int[] MAZE_SIZES = new int[] {5, 8, 10};
		private static readonly string SCENE_LEVEL_NAME = "Level";
		private static readonly string SCENE_MAIN_MENU_NAME = "MainMenu";

		private DifficultyLevel gameDifficulty;
		private LevelBuilder levelBuilder;
		private OSDManager osdManager;
		private GameMenuManager gameMenuManager;
		private LevelTimer levelTimer;
		private Jukebox jukebox;
		private AudioSource jukeboxSource;
		private MouseAimCamera _camera;
		private Player player;
		private float musicVolume;
		private float volume;
		private MazeSize mazeSizeSelection;
		private GameMode gameMode;
		private Maze maze;
		private Highscores highScores;
		private bool[,] visitedTiles;
		private Vector2 playerPosMaze;
		private string playerName;
		private bool paused;
		private bool levelRunning = false;
		private Save[] savedGames;
		private string[] savedGameTitles;
		private int resolutionChoice; //The index of the choosen resolution in Screen.resolutions
		private bool fullscreen;
		private string gameToLoad;
		private Vector3 activeRespawnPoint;
		private Vector3 nextRespawnPoint;

		/// <summary>
		/// Struct, die zur serialisierung von Spielständen verwendet wird.
		/// </summary>
		[Serializable]
		private struct SerializationBundle {
			/// <summary>
			/// Das Labyrinth.
			/// </summary>
			public Maze maze;
			/// <summary>
			/// Die Highscores.
			/// </summary>
			public Highscores scores;
			/// <summary>
			/// The difficulty.
			/// </summary>
			public DifficultyLevel difficulty;

			/// <summary>
			/// IInitialisiert eine neue Instanz der <see cref="Sabbac.GameManager+SerializationBundle"/> struct.
			/// </summary>
			/// <param name="m">Das Labyrinth.</param>
			/// <param name="s">Die Highscores.</param>
			/// <param name="d">Der Schwierigkeitsgrad</param>
			public SerializationBundle(Maze m, Highscores s, DifficultyLevel d) {
				maze = m;
				scores = s;
				difficulty = d;
			}
		}

		protected GameManager() {
			this.gameToLoad = "";
			if (!FetchPrefs ()) {
				InitGameData();
			}
		}

		// Use this for initialization
		void Awake () {
			Time.timeScale = 1.0f;
		}

		/// <summary>
		/// Erzeugt das Level nach laden der Szene.
		/// </summary>
		void OnLevelWasLoaded() {
			Time.timeScale = 1.0f;
			/**
		 	 * Initialise variables once a level was loaded
		 	 */
			if (Application.loadedLevelName == SCENE_LEVEL_NAME) {
				GameObject gc = GameObject.Find ("GameController");
				GameObject cam = GameObject.Find ("MouseAimCamera");
				GameObject pl = GameObject.Find ("Player");
				this.levelBuilder = (LevelBuilder)gc.GetComponent (typeof(LevelBuilder));
				this.osdManager = (OSDManager)cam.GetComponent (typeof(OSDManager));
				this.gameMenuManager = (GameMenuManager)gc.GetComponent (typeof(GameMenuManager));
				this.levelTimer = (LevelTimer)gc.GetComponent (typeof(LevelTimer));
				this.jukebox = (Jukebox)cam.GetComponent (typeof(Jukebox));
				this.jukeboxSource = (AudioSource)cam.GetComponent (typeof(AudioSource));
				this._camera = (MouseAimCamera)cam.GetComponent (typeof(MouseAimCamera));
				this.player = (Player)pl.GetComponent(typeof(Player));

				if (this.gameMode == GameMode.MODE_RANDOM_MAZE) {
					InitWithRandomLevel ();
				} else {
					InitFromSave ();			
				}
			
				this.levelBuilder.BuildLevelWithMaze (this.maze);
				this.visitedTiles = new bool[this.maze.GetHeight (), this.maze.GetWidth ()];
				this.playerPosMaze = new Vector2 (0, 0);
				this.levelTimer.StartTimer ();
			
				this.jukebox.volume = this.musicVolume * 100;
				this.jukeboxSource.ignoreListenerPause = true;
				this.jukeboxSource.ignoreListenerVolume = true;
			
				this.paused = false;
				this.levelRunning = true;
			}
		}

		/// <summary>
		/// Läd die Level Szene und beginnt das Spiel.
		/// </summary>
		public void StartLevel() {
			Application.LoadLevel(SCENE_LEVEL_NAME);
		}

		/// <summary>
		/// Callback, verwendet um dem GameManager die Position des Spielers innerhalb des Labyrinths mitzuteilen.
		/// </summary>
		/// <param name="x">Die X Koordinate.</param>
		/// <param name="y">Die Y Koordinate.</param>
		/// <param name="respawnPoint">Der Respawn Punkt.</param>
		public void ReportPlayerPosition(int x, int y, Vector3 respawnPoint) {
			this.activeRespawnPoint = this.nextRespawnPoint;
			this.nextRespawnPoint = respawnPoint;

			this.visitedTiles [x, y] = true;
			this.playerPosMaze.x = x;
			this.playerPosMaze.y = y;
		}

		/// <summary>
		/// Callback, verwendet um dem GameManager das verlieren des Levels mitzuteilen.
		/// </summary>
		public void FailLevel() {

			if (!this.player.IsRespawning ()) {

				bool lived = this.player.LooseLive ();

				StartCoroutine (HelperFunctions.DoAfterWait (2.0f, delegate() {
					if (!lived) {
						EndGame (false);			
					} else {
						this.player.Respawn (this.activeRespawnPoint);		
					}
				}));
			}
		}

		/// <summary>
		/// Zeigt an ob gerade ein Level gespielt wird.
		/// </summary>
		/// <returns><c>true</c>, wenn gerade ein Level gespielt wird. <c>false</c>, wenn der Spieler bereits gewonnen hat, oder sich im Hauptmenü befindet.</returns>
		public bool LevelIsRunning() {
			return this.levelRunning;		
		}

		/// <summary>
		/// Liefert das verwendete Labyrinth.
		/// </summary>
		/// <returns>Das Labyrinth.</returns>
		public Maze GetMaze(){
			return this.maze;
		}

		/// <summary>
		/// Liefert die Position des Spielers innerhalb des Labyrinths.
		/// </summary>
		/// <returns>Die Position.</returns>
		public Vector2 GetPlayerPosMaze() {
			return this.playerPosMaze;
		}

		/// <summary>
		/// Liefert die verstrichene Zeit des Levels.
		/// </summary>
		/// <returns>Die verstrichene Zeit.</returns>
		public float GetLevelTime() {
			return this.levelTimer.GetElapsedTime ();
		}

		/// <summary>
		/// Liefert die Highscores.
		/// </summary>
		/// <returns>Die Highscores.</returns>
		public Highscores GetHighscores() {
			return this.highScores;
		}

		/// <summary>
		/// Liefert den Spielstand an dem übergebenen Index
		/// </summary>
		/// <returns>Der Spielstand.</returns>
		/// <param name="i">Der Index.</param>
		public Save GetSavedGame(int i) {
			Save s = new Save("", "");

			if (i >= 0 && i < this.savedGames.Length) {
				s = this.savedGames[i];			
			}

			return s;		
		}

		/// <summary>
		/// Liefert die Titel der gespeicherten Spiele.
		/// </summary>
		/// <returns>Die Titel.</returns>
		public string[] GetSavedGameTitles() {
			return this.savedGameTitles;		
		}

		/// <summary>
		/// Liefert die verwendete Musiklautstärke.
		/// </summary>
		/// <returns>Die Lautstärke.</returns>
		public float GetMusicVolume() {
			return this.musicVolume;		
		}

		/// <summary>
		/// Liefert die verwendete EffektLautstärke.
		/// </summary>
		/// <returns>Die Effektlautstärke.</returns>
		public float GetEffectsVolume() {
			return this.volume;		
		}

		/// <summary>
		/// Liefert den Namen des Spielers.
		/// </summary>
		/// <returns>Der Name.</returns>
		public string GetPlayerName() {
			return this.playerName;		
		}

		/// <summary>
		/// Liefert den Index der ausgewählten Auflösung.
		/// </summary>
		/// <returns>Der Index.</returns>
		public int GetResolutionChoice() {
			return this.resolutionChoice;		
		}

		/// <summary>
		/// Liefert den aktuell ausgewählte Schwierigkeit.
		/// </summary>
		/// <returns>Die Schwierigkeit.</returns>
		public DifficultyLevel GetDifficulty() {
			return this.gameDifficulty;		
		}

		/// <summary>
		/// Liefert die aktuelle Labyrinthgröße.
		/// </summary>
		/// <returns>Die Labyrinthgröße.</returns>
		public MazeSize GetMazeSize() {
			return this.mazeSizeSelection;		
		}

		/// <summary>
		/// Liefert die Daten des zu ladenen Spiels.
		/// </summary>
		/// <returns>Das zu ladende Spiel.</returns>
		public string GetGameToload() {
			return this.gameToLoad;		
		}

		/// <summary>
		/// Liefert den Spielercharakter.
		/// </summary>
		/// <returns>Der Spieler.</returns>
		public Player GetPlayer() {
			return this.player;		
		}

		/// <summary>
		/// Zeigt an, ob das Spiel im Vollbildmodus ausgeführt wird.
		/// </summary>
		/// <returns><c>true</c>, wenn das Spiel im Vollbildmodus ist, sonst <c>false</c>.</returns>
		public bool IsFullscreen() {
			return this.fullscreen;		
		}

		/// <summary>
		/// Aktiviert, oder deaktiviert den Vollbildmodus des Spiels.
		/// </summary>
		/// <param name="f">Wenn <c>true</c> wird der Vollbildmodus aktiviert, sonst deaktiviert.</param>
		public void SetFullscreen(bool f) {
			if (f != IsFullscreen ()) {
				this.fullscreen = f;
				Resolution r = Screen.resolutions[GetResolutionChoice()];
				Screen.SetResolution(r.width, r.height, IsFullscreen(), r.refreshRate);
			}
		}

		/// <summary>
		/// Setzt den ausgewählten Spielmodus.
		/// </summary>
		/// <param name="mode">Der Spielmodus.</param>
		public void SetGameMode(GameMode mode) {
			this.gameMode = mode;		
		}

		/// <summary>
		/// Setzt die ausgewählte Labyrinthgröße.
		/// </summary>
		/// <param name="size">Die Labyrinthgröße.</param>
		public void SetMazeSize(MazeSize size) {
			this.mazeSizeSelection = size;		
		}

		/// <summary>
		/// Wählt den Schwirigkeitsgrad aus.
		/// </summary>
		/// <param name="difficulty">Der Schwierigkeitsgrad.</param>
		public void SetDifficulty(DifficultyLevel difficulty) {
			this.gameDifficulty = difficulty;		
		}

		/// <summary>
		/// Setzt die verwendete Musiklautstärke.
		/// </summary>
		/// <param name="volume">Die Musiklautstärke in Prozent.</param>
		public void SetMusicVolume(float volume) {
				this.musicVolume = Mathf.Clamp(volume, 0, 1);
		}

		/// <summary>
		/// Beeinflusst die Musiklautstärke um das übergebene delta.
		/// </summary>
		/// <param name="deltaVolume">Delta Lautstärke.</param>
		public void ChangeMusicVolume(float deltaVolume) {
			SetMusicVolume(this.musicVolume + deltaVolume);
		}

		/// <summary>
		/// Setzt die verwendete Effektlautstärke.
		/// </summary>
		/// <param name="volume">Die effektlautstärke in Prozent.</param>
		public void SetEffectsVolume(float volume) {
			this.volume = Mathf.Clamp (volume, 0, 1);
			AudioListener.volume = volume;	
		}

		/// <summary>
		/// Beeinflusst die Effektlautstärke um das übergebene delta.
		/// </summary>
		/// <param name="deltaVolume">Delta Lautstärke.</param>
		public void ChangeEffectsVolume(float deltaVolume) {
			SetEffectsVolume(this.volume + deltaVolume);
		}

		/// <summary>
		/// Setzt den verwendeten Spielernamen.
		/// </summary>
		/// <param name="name">Der Spielername.</param>
		public void SetPlayerName(string name) {
			this.playerName = name;	
		}

		/// <summary>
		/// Setzt die verwendete Auflösung auf die Auflösung des übergebenen Index.
		/// </summary>
		/// <param name="choice">Der Index.</param>
		public void SetResolution(int choice) {
			if (choice >= 0 && choice < Screen.resolutions.Length) {
				this.resolutionChoice = choice;

				Resolution r = Screen.resolutions[GetResolutionChoice()];
				Screen.SetResolution(r.width, r.height, IsFullscreen(), r.refreshRate);
			}
		}

		/// <summary>
		/// Wählt das zu ladende Spiel aus.
		/// </summary>
		/// <param name="index">Index des zu ladenden Spiels.</param>
		public void SetGameToLoad(int index) {
			if (index >= 0 && index < this.savedGames.Length) {
				this.gameToLoad = this.savedGames[index].data;			
			}
		}

		/// <summary>
		/// Setzt das zu Ladende Spiel auf die übergebenen Spieldaten.
		/// </summary>
		/// <param name="data">Die Spieldaten, Base64 encodiert.</param>
		public void SetGameToLoad(string data) {
			this.gameToLoad = data;		
		}

		/// <summary>
		/// Zeigt an ob das Labyrintfeld an der übergebenen Position bereits von dem Spieler besucht wurde.
		/// </summary>
		/// <returns><c>true</c>, wenn das angegebene Feld bereits besucht wurde, sonst <c>false</c>.</returns>
		/// <param name="x">Die X Koordinate.</param>
		/// <param name="y">Die Y koordinate.</param>
		public bool HasBeenVisited(int x, int y) {
			return this.visitedTiles [x, y];
		}

		/// <summary>
		/// Pausiert das aktuelle Spiel.
		/// </summary>
		public void PauseGame() {
			this.paused = true;
			this.levelTimer.TogglePause ();
			this._camera.TogglePause ();
			Time.timeScale = 0.0f;
			this.osdManager.ToggleOSD ();
			this.gameMenuManager.DisplayPauseMenu ();
		}

		/// <summary>
		/// Setzt das aktuelle Spiel fort.
		/// </summary>
		public void ResumeGame() {
			this.paused = false;
			this.gameMenuManager.DisableMenu ();
			this.osdManager.ToggleOSD ();
			Time.timeScale = 1.0f;
			this._camera.TogglePause ();
			this.levelTimer.TogglePause ();
		}

		/// <summary>
		/// Wechselt den Zustand des Spiel zwischen pausiert und nicht pausiert.
		/// </summary>
		public void TogglePause() {
			if (this.paused) {
				ResumeGame ();		
			} else {
				PauseGame ();		
			}
		}

		/// <summary>
		/// Startet das aktuelle Level neu.
		/// </summary>
		public void RestartLevel() {
			SetGameToLoad(SerializeGame ());
			SetGameMode (GameMode.MODE_LOAD_MAZE);
			DestroyGameComponents ();
			StartLevel ();
		}

		/// <summary>
		/// Läd das Hauptmenü.
		/// </summary>
		public void LoadMainMenu() {
			this.levelRunning = false;
			DestroyGameComponents ();
			Application.LoadLevel(SCENE_MAIN_MENU_NAME);		
		}

		/// <summary>
		/// Speichert das aktuelle Spiel.
		/// </summary>
		/// <param name="name">Name des Speicherstands.</param>
		/// <param name="slot">Slot, indem der Speicherstand abgelegt werden soll.</param>
		public void SaveGame(String name, int slot) {
			if (slot >= 0 && slot < this.savedGames.Length) {
				string data = SerializeGame ();
				this.savedGames[slot] = new Save(name, data);
				this.savedGameTitles[slot] = name;
			}
			PlayerPrefs.SetString (PREFS_SAVES_DESTINATION, SerializeSaves (this.savedGames));
		}

		/// <summary>
		/// Serialisiert das aktuelle Spiel.
		/// </summary>
		/// <returns>Die daten des aktuellen Spiels, Base64 encodiert.</returns>
		public string SerializeGame() {
			SerializationBundle b = new SerializationBundle (this.maze, this.highScores, this.gameDifficulty);
			BinaryFormatter bf = new BinaryFormatter ();
			MemoryStream ms = new MemoryStream();

			bf.Serialize (ms, b);

			return Convert.ToBase64String (ms.ToArray ());
		}

		/// <summary>
		/// Deserialisiert die übergebenen Spieldaten.
		/// </summary>
		/// <returns>Die Spieldaten.</returns>
		/// <param name="state">Der Speicherstand, Base64 encodiert.</param>
		private SerializationBundle DeserializeGame(string state) {
			SerializationBundle bundle = new SerializationBundle();
			try {
				byte[] b = Convert.FromBase64String (state);
				MemoryStream ms = new MemoryStream ();
				BinaryFormatter bf = new BinaryFormatter ();

				ms.Write(b, 0, b.Length);
				ms.Seek (0, SeekOrigin.Begin);

				bundle = (SerializationBundle)bf.Deserialize (ms);
			} catch (Exception e) {
				Debug.LogException(e);
				int size = MAZE_SIZES [(int)MazeSize.Size_Small];
				bundle.maze = MazeGenerator.GenerateMaze (size, size);
				bundle.scores = new Highscores ();
			}
			return bundle;
		}

		/// <summary>
		/// Serialisiert die übergebenen Speicherstände.
		/// </summary>
		/// <returns>Base64 encodierte Speicherstände.</returns>
		/// <param name="saves">Speicherstände.</param>
		public static string SerializeSaves(Save[] saves) {
			BinaryFormatter bf = new BinaryFormatter ();
			MemoryStream ms = new MemoryStream ();

			bf.Serialize (ms, saves);

			return Convert.ToBase64String (ms.ToArray ());
		}

		/// <summary>
		/// Deserialisiert die übergebenen Speicherstanddaten.
		/// </summary>
		/// <returns>Die Speicherstände.</returns>
		/// <param name="s">Die Base64 encodierten Speicherstände.</param>
		public static Save[] DeserializeSaves(string s) {
			Save[] saves = null;
			try {
				byte[] b = Convert.FromBase64String (s);
				MemoryStream ms = new MemoryStream ();
				BinaryFormatter bf = new BinaryFormatter ();

				ms.Write(b, 0, b.Length);
				ms.Seek (0, SeekOrigin.Begin);
			
				saves = (Save[])bf.Deserialize (ms);
			} catch (Exception e) {
				Debug.LogException(e);			
			}

			return saves;
		}

		/// <summary>
		/// Speichert die Einstellungen in den PlayerPrefs.
		/// </summary>
		public void SaveSettings() {
			PlayerPrefs.SetInt (PREFS_GAME_PREFIX, 1);
			PlayerPrefs.SetFloat (PREFS_MUSIC_VOLUME_DESTINATION, this.musicVolume);
			PlayerPrefs.SetFloat (PREFS_VOLUME_DESTINATION, this.volume);
			PlayerPrefs.SetInt (PREFS_RESOLUTION_CHOICE_DESTINATION, this.resolutionChoice);
			PlayerPrefs.SetString (PREFS_PLAYER_NAME_DESTINATION, this.playerName);
			PlayerPrefs.Save ();
		}

		/// <summary>
		/// Läd die Einstellungen von den PlaerPrefs.
		/// </summary>
		public void LoadSettings() {
			SetMusicVolume(PlayerPrefs.GetFloat (PREFS_MUSIC_VOLUME_DESTINATION));
			SetEffectsVolume(PlayerPrefs.GetFloat (PREFS_VOLUME_DESTINATION));
			SetPlayerName(PlayerPrefs.GetString (PREFS_PLAYER_NAME_DESTINATION));
			SetResolution (PlayerPrefs.GetInt (PREFS_RESOLUTION_CHOICE_DESTINATION));
		}

		/// <summary>
		/// Beendet das aktive Spiel.
		/// </summary>
		/// <param name="win">Zeigt an, ob das Spiel gewonnen wurde.</param>
		public void EndGame(bool win) {
			this.paused = true;
			this.levelRunning = false;
			this.levelTimer.TogglePause ();
			this.osdManager.ToggleOSD ();
			StartCoroutine (HelperFunctions.DoAfterWait (1.0f, delegate() {
				this._camera.TogglePause ();
				Time.timeScale = 0.0f;
				if (win) {
					Highscores.Score score = new Highscores.Score (this.playerName, this.levelTimer.GetElapsedTime ());
					bool highscore = this.highScores.IsHighScore (score);
					gameMenuManager.DisplayWonMenu (highscore);
					if (highscore) {
						this.highScores.AddScore (score);
					}
				} else {
						gameMenuManager.DisplayLostMenu ();
				}
			}));
		}

		/// <summary>
		/// Wendet die Effekte des übergebenen PowerUps an.
		/// </summary>
		/// <param name="p">Das PowerUp.</param>
		public void ApplyPowerUp(PowerUp p) {
			switch (p.GetPowerUpType ()) {
			case PowerUpType.Type_TimeReduction:
				this.levelTimer.ReduceElapsedTime(PowerUp.TIME_REDUCTION_EFFECT);
				break;

			case PowerUpType.Type_SpeedBoost:
				StartCoroutine(this.player.BoostSpeed(PowerUp.SPEED_BOOST_EFFECT, PowerUp.SPEED_BOOST_DURATION));
				break;

			case PowerUpType.Type_BonusLive:
				this.player.AddBonusLive();
				break;

			case PowerUpType.Type_NoGravity:
				StartCoroutine(this.player.NoGravity(PowerUp.NO_GRAVITY_DURATION));
				break;
			}
		}

		/// <summary>
		/// Initialisiert das Spiel mit einem zufälligen Level.
		/// </summary>
		private void InitWithRandomLevel() {
			int size = MAZE_SIZES [(int)this.mazeSizeSelection];
			this.maze = MazeGenerator.GenerateMaze (size, size);
			this.highScores = new Highscores ();
		}

		/// <summary>
		/// Initialisiert das Spiel von einem Speicherstand.
		/// </summary>
		private void InitFromSave(){
			if (this.gameToLoad != "") {
				SerializationBundle sb = DeserializeGame (this.gameToLoad);

				this.maze = sb.maze;
				this.highScores = sb.scores;
				this.gameDifficulty = sb.difficulty;
			} else {
				this.mazeSizeSelection = MazeSize.Size_Small;
				InitWithRandomLevel();			
			}
		}

		/// <summary>
		/// Initialisiert die Spieldaten für den ersten Start.
		/// </summary>
		private void InitGameData() {
			SetMusicVolume(0.5f);
			SetEffectsVolume(0.5f);
			SetDifficulty(DifficultyLevel.Level_Easy);
			SetMazeSize(MazeSize.Size_Small);
			this.resolutionChoice = 0;
			SetPlayerName("Player 1");
			SaveSettings();
			
			this.savedGames = new Save[5];
			this.savedGameTitles = new string[this.savedGames.Length + 1];
			for (int i = 0; i < this.savedGames.Length; i++) {
				this.savedGameTitles [i] = this.savedGames [i].name;
			}
			this.savedGameTitles [this.savedGames.Length] = "Geteiltes Spiel laden";
			string s = SerializeSaves(this.savedGames);
			PlayerPrefs.SetString(PREFS_SAVES_DESTINATION, s);
			PlayerPrefs.Save();		
		}

		/// <summary>
		/// Ruft die PlayerPrefs ab.
		/// </summary>
		/// <returns><c>true</c>, wenn das Spiel schon einmal gestartet wurde, sonst <c>false</c>.</returns>
		private bool FetchPrefs() {

			bool runBefore = PlayerPrefs.HasKey(PREFS_GAME_PREFIX);

			SetMazeSize((MazeSize)PlayerPrefs.GetInt(PREFS_MAZE_SIZE_DESTINATION));
			SetDifficulty((DifficultyLevel)PlayerPrefs.GetInt (PREFS_DIFFICULTY_DESTINATION));

			LoadSettings ();

			if (runBefore) {
				string s = PlayerPrefs.GetString (PREFS_SAVES_DESTINATION);
				this.savedGames = DeserializeSaves (s);
				this.savedGameTitles = new string[this.savedGames.Length + 1];
				for (int i = 0; i < this.savedGames.Length; i++) {
					this.savedGameTitles [i] = this.savedGames [i].name;
				}
				this.savedGameTitles [this.savedGames.Length] = "Geteiltes Spiel laden";
			}
			return runBefore;
		}

		/// <summary>
		/// Löscht die referenzierten Spielkomponenten.
		/// </summary>
		private void DestroyGameComponents() {

			Destroy (this.levelBuilder);
			Destroy(this.osdManager);
			Destroy (this.gameMenuManager);
			Destroy(this.levelTimer);
			Destroy (this.jukebox);
			Destroy(this.jukeboxSource);
			Destroy (this._camera);
			Destroy (this.player);
			Destroy (GameObject.Find("MouseAimCamera"));
			Destroy (GameObject.Find("GameController"));
			Destroy (GameObject.Find ("Player"));


			this.levelBuilder = null;
			this.osdManager = null;
			this.gameMenuManager = null;
			this.levelTimer = null;
			this.jukebox = null;
			this.jukeboxSource = null;
			this._camera = null;
			this.player = null;
		}
	}
}
